Example - Division
==================

    input '9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999'
    input_len 112 bytes

from base 10 to base 21

    result '8c4c46h83i5e3bjgf0j7ade67aha29ch063a751fj6d24hhh4655085df7c6103bi64c34086kbj2ca9d83h3'
    result_len 85

Encoding
--------

The input string encoded has 6 parts:

	d[5]='0x10a1f5b8132466'
	d[4]='0x53c07c59ed78c09b'
	d[3]='0xb60e94fde0330f22'
	d[2]='0x12ea2eebee3d257e'
	d[1]='0x5e40ffffffffffff'
	d[0]='0xffffffffffffffff'


Divisor
-------

We retrieve div_base information for base 21:

    div_base.exp    = 0xe                   (14)
    div_base.base   = 0x2d04b7fdd9c0ef49    (3243919932521508681)
    div_info.shift  = 0x2                   (2)

Round (5)
---------

### Computing quotients ###

We start with the most significant chunk, use it as high part, while the next
chunk we use it as a lower part. Use `divq` to divide two-word values with the divisor being stored 
in `div_info.base`. On each iteration we replace the high part with the remainder of the 
previous division operation.

div_base.base = 0x2d04b7fdd9c0ef49

| H                     |       L                   |       Q               |       R               |
|-----------------------|---------------------------|-----------------------|-----------------------|
| 0x10a1f5b8132466      | 0x53c07c59ed78c09b        | 0x5e9524780d5e96      | 0x1a41d56d1fdabdd5    |
| 0x1a41d56d1fdabdd5    | 0xb60e94fde0330f22        | 0x95501ba712c88ea3    | 0x2c742466229835a7    |
| 0x2c742466229835a7    | 0x12ea2eebee3d257e        | 0xfcc9db5f42fdfa6f    | 0x128f50bfccc31ad7    |
| 0x128f50bfccc31ad7    | 0x5e40ffffffffffff        | 0x698aa36eda87cb94    | 0xfb7e8b1c537c6cb     |
| 0xfb7e8b1c537c6cb     | 0xffffffffffffffff        | 0x596232bc937cbade    | 0x11dcc3302a7974b1    |

While performing the divisions we replace the chunk value with the quotients:

    d[5]='0x0'
    d[4]='0x5e9524780d5e96'
    d[3]='0x95501ba712c88ea3'
    d[2]='0xfcc9db5f42fdfa6f'
    d[1]='0x698aa36eda87cb94'
    d[0]='0x596232bc937cbade'

### Computing remainders ###

Next we use the last remainder value and divide it with base output:

R = 0x11dcc3302a7974b1, div = 21

| Q                 | R             |
|-------------------|---------------|
| 0xd9c026dd737f76  | 0x3           |
| 0xa5e7bc1670611   | 0x11          |
| 0x7e676abbc356    | 0x3           |
| 0x604ecb39b96     | 0x8           |
| 0x49609ad6a5      | 0xd           |
| 0x37e81472c       | 0x9           |
| 0x2a98711a        | 0xa           |
| 0x2074256         | 0xc           |
| 0x18ba04          | 0x2           |
| 0x12d6d           | 0x13          |
| 0xe5a             | 0xb           |
| 0xae              | 0x14          |
| 0x8               | 0x6           |
| 0x0               | 0x8           |

Divisions for retrieving the remainders: 14. This value is tested against `div_base.exp`
in order to make sure that we have sufficient remainders -- the quotient might reach zero 
much faster so we pad store these remainders with the value zero.

Round (4)
---------

### Computing quotients ###


div_base.base = 0x2d04b7fdd9c0ef49

| H                     |       L                   |       Q               |       R               |
|-----------------------|---------------------------|-----------------------|-----------------------|
| 0x5e9524780d5e96      | 0x95501ba712c88ea3        | 0x219d97a5cfa6f68     | 0x168e64ea0b5cb1fb    |
| 0x168e64ea0b5cb1fb    | 0xfcc9db5f42fdfa6f        | 0x80446fd38ab10004    | 0x9a78bda10813d4b     |
| 0x9a78bda10813d4b     | 0x698aa36eda87cb94        | 0x36e69868b239bc1c    | 0x26efe7c350730398    |
| 0x26efe7c350730398    | 0x596232bc937cbade        | 0xdd6b167c5122326f    | 0x1042b2719966b837    |


While performing the divisions we replace the chunk value with the quotients:

    d[5]='0x0'
    d[4]='0x0'
    d[3]='0x219d97a5cfa6f68'
    d[2]='0x80446fd38ab10004'
    d[1]='0x36e69868b239bc1c'
    d[0]='0xdd6b167c5122326f'


### Computing remainders ###

R = 0x1042b2719966b837, div=21

| Q                 | R             |
|-------------------|---------------|
| 0xc639425ca35e1b  | 0x0           |
| 0x97070dfd7047b   | 0x4           |
| 0x731190c11898    | 0x3           |
| 0x57abdc00d5c     | 0xc           |
| 0x42cc1555f8      | 0x4           |
| 0x32e4a28aa       | 0x6           |
| 0x26c69438        | 0x12          |
| 0x1d8b1b9         | 0xb           |
| 0x16825e          | 0x3           |
| 0x11266           | 0x0           |
| 0xd11             | 0x1           |
| 0x9f              | 0x6           |
| 0x7               | 0xc           |
| 0x0               | 0x7           |

Divisions for retrieving the remainders: 14. Done so far, 28.

Round (3)
---------

### Computing quotients ###

div_base.base = 0x2d04b7fdd9c0ef49

| H                     |       L                   |       Q               |       R               |
|-----------------------|---------------------------|-----------------------|-----------------------|
| 0x219d97a5cfa6f68     | 0x80446fd38ab10004        | 0xbf2836cad8074b5     | 0xfa1c2b52f5abd67     |
| 0xfa1c2b52f5abd67     | 0x36e69868b239bc1c        | 0x58e44004fc2aa507    | 0x75550c745bf241d     |
| 0x75550c745bf241d     | 0xdd6b167c5122326f        | 0x29b3777bd0043f51    | 0x4c85611e30f8556     |

While performing the divisions we replace the chunk value with the quotients:

    d[5]='0x0'
    d[4]='0x0'
    d[3]='0x0'
    d[2]='0xbf2836cad8074b5'
    d[1]='0x58e44004fc2aa507'
    d[0]='0x29b3777bd0043f51'

### Computing remainders ###

R = 0x4c85611e30f8556, div = 21

| Q                 |   R           |
|-------------------|---------------|
| 0x3a4d3dcddc2aeb  | 0xf           |
| 0x2c6b9ccd9b8e6   | 0xd           |
| 0x21d815f1fc9d    | 0x5           |
| 0x19c9354aa81     | 0x8           |
| 0x13a57defbd      | 0x0           |
| 0xef7fe6d8        | 0x5           |
| 0xb679d47         | 0x5           |
| 0x8b077d          | 0x6           |
| 0x69ed5           | 0x4           |
| 0x50b4            | 0x11          |
| 0x3d7             | 0x11          |
| 0x2e              | 0x11          |
| 0x2               | 0x4           |
| 0x0               | 0x2           |

Divisions for retrieving the remainders: 14. Done so far, 42.

Round (2)
---------

### Computing quotients ###

div_base.base = 0x2d04b7fdd9c0ef49

| H                     |       L                   |       Q               |       R               |
|-----------------------|---------------------------|-----------------------|-----------------------|
| 0xbf2836cad8074b5     | 0x58e44004fc2aa507        | 0x43f06b2c37cd293f    | 0x2296da3832e81110    |
| 0x2296da3832e81110    | 0x29b3777bd0043f51        | 0xc4b1a569b6d47356    | 0x1499ef8def4211cb    |


While performing the divisions we replace the chunk value with the quotients:

    d[5]='0x0'
    d[4]='0x0'
    d[3]='0x0'
    d[2]='0x0'
    d[1]='0x43f06b2c37cd293f'
    d[0]='0xc4b1a569b6d47356'

### Computing remainders ###

R = 0x1499ef8def4211cb, div = 21

| Q                 |   R           |
|-------------------|---------------|
| 0xfb23c9ce70dc46  |  0xd          |
| 0xbf58383bc3b40   |  0x6          |
| 0x91c94f6a7cb9    |  0x13         |
| 0x6f1348b2a82     |  0xf          |
| 0x54a0fa6fbd      |  0x1          |
| 0x407aa66d8       |  0x5          |
| 0x31207ecd        |  0x7          |
| 0x256e177         |  0xa          |
| 0x1c84a4          |  0x3          |
| 0x15ba6           |  0x6          |
| 0x108e            |  0x0          |
| 0xc9              |  0x11         |
| 0x9               |  0xc          |
| 0x0               |  0x9          |

Divisions for retrieving the remainders: 14. Done so far, 56.

Round (1)
---------
 
### Computing quotients ###

H (0x43f06b2c37cd293f) >= DIVISOR (0x2d04b7fdd9c0ef49)

Changing divisor to `div_base.base << div_base.shift = 0xb412dff76703bd24`

Then lshift chunks with `div_base.shift = 0x2` and adjust higher part and 
lower part.

~~~~~~~~~~~
ul = (uh << info->shift);
~~~~~~~~~~~

=> L = 0x43f06b2c37cd293f << 0x2 = 0xfc1acb0df34a4fc

Perform a left shift with 0x2:

~~~~~~~~~~~
chunk_lshift(chunk, chunk, 1, info->shift);
~~~~~~~~~~~

Contents of the chunks now looks like this:

    d[5]='0x0'
    d[4]='0x0'
    d[3]='0x0'
    d[2]='0x0'
    d[1]='0x43f06b2c37cd293f'
    d[0]='0x12c695a6db51cd58'       <-- was left shifted from 0xc4b1a569b6d47356

Add the result of lsfhit with previous value:

~~~~~~~~~~~
ul |= chunk_lshift(chunk, chunk, n, info->shift);
~~~~~~~~~~~

=> L = 0xfc1acb0df34a4fc | (0xc4b1a569b6d47356 >> (64 - 0x2))
     = 0xfc1acb0df34a4fc | 0x3 = 0xfc1acb0df34a4ff

~~~~~~~~~~~
uh >>= (CHUNK_BITS - info->shift);
~~~~~~~~~~~


=> H = H >> (64 - 0x2) = 0x43f06b2c37cd293f >> 62 = 0x1

And finally the new values are:

    H = 0x1, L = 0xfc1acb0df34a4ff

Perform a division using div = 0xb412dff76703bd24

Using these values we determine the remainder

| H                     |       L                   |       Q               |       R               |
|-----------------------|---------------------------|-----------------------|-----------------------|
| 0x1                   | 0xfc1acb0df34a4ff         | 0x1                   | 0x5baeccb97830e7db    |

Now the high part will have the contents of the remainder

=> H = 0x5baeccb97830e7db

And we also replace the base with

div_base.base = 0xb412dff76703bd24

| H                     |       L                   |       Q               |       R               |
|-----------------------|---------------------------|-----------------------|-----------------------|
| 0x5baeccb97830e7db    | 0x12c695a6db51cd58        | 0x82570681c43e6584    | 0x8f578795e50d12c8    |

While performing the divisions we replace the chunk value with the quotients:

    d[5]='0x0'
    d[4]='0x0'
    d[3]='0x0'
    d[2]='0x0'
    d[1]='0x1'                  <- one more round (replaced above)
    d[0]='0x82570681c43e6584'


### Computing remainders ###

Right shift the remainder `R = 0x8f578795e50d12c8` with `div_base.shift = 0x2`

R=0x23d5e1e5794344b2, div = 21

| Q                 |   R           |
|-------------------|---------------|
| 0x1b4d9febca1adf0 | 0x2           |
| 0x14cd6da77569ce  | 0xa           |
| 0xfd9781e10509    | 0x11          |
| 0xc1367b561c3     | 0xa           |
| 0x9335b34dcc      | 0x7           |
| 0x7028ea22e       | 0x6           |
| 0x557481a0        | 0xe           |
| 0x411bd07         | 0xd           |
| 0x319b49          | 0xa           |
| 0x25cba           | 0x7           |
| 0x1ccb            | 0x13          |
| 0x15f             | 0x0           |
| 0x10              | 0xf           |
| 0x0               | 0x10          |

Divisions for retrieving the remainders: 14. Done so far, 70.

Round (still round 1)
---------------------

### Computing quotients ###

div_base = 0x2d04b7fdd9c0ef49

| H                     |       L                   |       Q               |       R               |
|-----------------------|---------------------------|-----------------------|-----------------------|
| 0x1                   | 0x82570681c43e6584        | 0x8                   | 0x1a314692f636eb3c    |

While performing the divisions we replace the chunk value with the quotients:

    d[5]='0x0'
    d[4]='0x0'
    d[3]='0x0'
    d[2]='0x0'
    d[1]='0x0'
    d[0]='0x8'


### Computing remainders ###

R = 0x1a314692f636eb3c, div = 21

| Q                 |   R           |
|-------------------|---------------|
| 0x13f4c80e7272fc5	| 0x13          |
| 0xf3467a97bc552	| 0xb           |
| 0xb95a4505e4d3	| 0x3           |
| 0x8d38961cdf1	    | 0xe           |
| 0x6b98d3e53c	    | 0x5           |
| 0x51fa89102	    | 0x12          |
| 0x3e75bdc3	    | 0x3           |
| 0x2f96a8f	        | 0x8           |
| 0x244206	        | 0x11          |
| 0x1ba00	        | 0x6           |
| 0x150c	        | 0x4           |
| 0x100	            | 0xc           |
| 0xc               | 0x4           |
| 0x0               | 0xc           |

Divisions for retrieving the remainders: 14. Done so far, 84.

Round (0)
---------

### Computing quotients ###

No more rounds to do. H = 0x8.

### Computing remainders ###

R = 0x8

| Q                 |   R           |
|-------------------|---------------|
| 0x0               | 0x8           |

Divisions for retrieving the remainders: 1. Done so far, 85.
