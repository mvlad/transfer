Division
========

To generate the output, we inverse the remainders -- result from division
operations, and by using the English-alphabet (`0123456789abcdefghijklmnopqrstuvwxyz`) 
we convert the remainders to a char array representation, useful 
for writing to an output file.

We use two methods for divisions, depending on the output base:

*  __convert_pow_of_2    -- when the output base is a power of 2
*  __convert_str         -- naive version, otherwise

Calculating output string size
------------------------------

We need to figure out how big the converted string will be. A conversion
function -- such as  __convert_str , needs to determine how big the
result will be. Each potential conversion function will make uses it's own method 
(i.e., division method) to determine how many remainders needs to be
calculated.

This ultimately leads to use the same division method(s) to determine:

1. the length of the output string
2. the remainders themselves

Doing the same thing twice will obviously have a negative impact when large
inputs are being used, therefore we make an approximation ( sz_approx
) of the output length, trading some (arguably small) memory for
speed.

Computing remainders (base not pow of 2)
----------------------------------------

### Computing the divisor ###

As we normalize the input string with respect to `base_from` we need to
determine the contents of `div_base` for `base_to`, before performing the divisions.
After that, we first divide the chunks with the `div_base.base` for `base_to`
we want to convert, and the last remainder result of this operation is 
further divided with the `base_to` for storing the remainders.

### Naive division ###

Hand-school division works by dividing the quotient until it reaches 0 and store
each remainder result.

1. we start with the most significant chunk, use it as high part, while the next
chunk we use it as a lower part. On on each iteration we replace the high part
with the remainder of the previous division operation ( chunk_div_naive ).
The number of iterations equals the number of chunks currently stored. 

2. while performing this operation the contents of the quotients for each division
replaces the chunk values  ( chunk_div ) and normally, at the end of the 
iteration the quotient should be zero.

3. the last remainder of this operation (when the
quotient reaches zero) is then taken and divided with `base_to`, storing
the remainders ( __cnv_str ).

4. if the quotient reaches zero much faster that the expected number of divisions --
from step 3, we pad add remainders with a zero value.

#### Adjusting the divisor ####

In each round we test weather the previous operation resulted in the quotient
being zero, by comparing if the current high part (taken from the chunks) is 
greater or equal with the divisor.

Example
-------

See [example](DIV_EXP.md).
