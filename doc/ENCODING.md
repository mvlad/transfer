Enconding
=========

The number is encoded to an internal representation, and division
operations are performed on that data by converting the input char array to an
array of integers.  We use to represent the input string (the big input number)
as a series of `unsigned long`s with the last entry in the array being the
most significant part (hax note, just like [GMP](https://gmplib.org/) library does):

    chunk[n - 1], chunk[n - 2], ..., chunk[1], chunk[0]

For instance, the `char` array (in decimal)
`14867163467071694589724647718821644187074559` will be represented
internally (on `x86_64` platform) as:

| chunk #   | char array              | unsigned long       |
|-----------|-------------------------|---------------------|
| 2         | `148671`                | `0xaaaa`            |
| 1         | `6346707169458972464`   | `0xaaaaeeeeeeeebbbb`|
| 0         | `7718821644187074559`   | `0xbbbbddddddddffff`|

The are methods responsible for encoding:

*  __encode_pow_of_2  when the base to convert
is a power of two
*  __encode  otherwise


1. Conversion to a byte-array
-----------------------------

Before converting the input string to a series of integers we need
to convert it first to a byte-array (`unsigned char`) array, which allows 
to perform integer operations over them.

2. Determine the number of chunks
---------------------------------

To find out how many parts are required for encoding the string into
chunks, we require:

* the length of the input string 
* the maximum number of digits can be stored into a unsigned long and store in
 div_base  data structure, with respect to `base_to` we
want to convert. `get_div_base`  is responsible for populating the
contents of the data structure.

~~~~~~~~
struct div_base {

    /* the divisor */
    unsigned long int base;

    /* exponent, the amount which we split the input string */
    unsigned int exp;

    /* in case the chunk size is bigger than the 
       divisor we need to adjust (normalize)  */

    unsigned int shift;
};
~~~~~~~~

We then use the exponent to calculate how many parts we need to allocate:

~~~~~~~~
allocate = (len_str + div_base.exp - 1) / div_base.exp;
~~~~~~~~

3. String splitting
-------------------

Converting the input string works basically like a `stroul(3)` function. We
take a slice of the byte-array and convert it to a integer:

~~~~~~~~~~~
integer = prev_integer * base_from + char;
~~~~~~~~~~~

then we perform the conversion of the input string in two stages:

- determine the least significant chunk 
-- and we need to determine how much to take from the input in the beginning:

~~~~~~~~~~
take_first = ((len_str - 1) % div_base.exp) + 1;
~~~~~~~~~~

- for each subsequent split operation (`div.exp` in length out of the byte array) we
*adjust* the chunks contents to accommodate for the new input we just processed.
Rinse and repeat until we get to the end.

4. Computing chunks values (base not pow of 2)
----------------------------------------------

Once we have a slice of the input string under the form of a integer 
(`div_base.exp` in size) we multiply the current chunk value with `div_base.base` 
and add the converted converted number to it.

### Multiplication ###

Multiply the current stored chunk(s) value with `div_base.base`. 

The product is computed using native `mulq` function which allows to multiply
two-words (the chunk itself and the base) values (of `sizeof(unsigned long)` in
size). The result is split into a high, and low part. Upon the first
iteration of the multiplication, there are no other chunks and the value of the
lower part is stored in the current chunk. The high part (saved in a
temporary variable) is being used on the next step of the multiplication. At the
next iteration, the current chunk is being multiplied with
`div_base.base`, with the product being split into a high a low part. The
lower part is being added with previous high part and the result is stored in
the current chunk, while the higher part is being saved for the next iteration. If
there are no more iterations left, the high part is being returned, and is
being feed to the addition step.

### Addition ###

Add the least significant stored chunk with the current converted integer.  If
there is more than one chunk, the next round of the addition will
depended on the result of the previous one:
if the result of the operation if smaller than the current converted integer 
we modify the current converted integer to 1, otherwise we replace it with 0. 
This result will be feed in the next iteration of the addition. If there are
no more chunks we return the modified value.


### Storing the high part ###

The result of the multiplication is being added with the result of the addition.
We increment the number of chunks and store this value as the most significant
chunk value.

Example
-------

Short [example](ENC_EXP.md).

