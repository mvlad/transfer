Example - Encoding
==================

- Input '14867163467071694589724647718821644187074559' to base 10.
- length 44 bytes

## Determine divisor ##

- div_base 


    div_base.base   = 0x8ac7230489e80000    (1000000000000000 decimal)
    div_base.exp    = 0x13                  (19 decimal)
    div_base.shift  = 0x0                   (0)

    10^19 = 1000000000000000 (0x8ac7230489e80000)

## No. of chunks ##

    allocate = (len_str + div_base.exp - 1) / div_base.exp = 44 + 18 / 19 = 62 / 19 = 3 chunks

## Initial split ##

    take_first = ((len_str - 1) % div_base.exp) + 1 = 43 % 19 + 1 = 5 + 1 = 6 bytes

## Round 0 ##

- conversion to integer

    '148671' -> '0x244bf' 

- storing

    chunks[0] = 0x244bf (j = 6)

## Round 1 ##

- conversion to integer

    '6346707169458972464' => '0x5814088b5906e730'

### Multiplication ###

- mul previous chunk with div_base.base

    chunks[0] * div_base.base = 
    0x244bf * 0x8ac7230489e80000 = 0x13ad2 (H) | 0xb5e968d484180000 (L)

- add lower part with previous high part

    0xb5e968d484180000 + 0x0 = 0xb5e968d484180000

- store low part in chunks[0]

    chunks[0] = 0xb5e968d484180000

- new high part = 0x13ad2 (lp > current high part 0x0)

- return new high part -> 0x13ad2

### Addition ###

- add chunks[0] (lp) with converted number

    0xb5e968d484180000 + 0x5814088b5906e730 = 0xdfd715fdd1ee730

- store the result in chunks[0]

    chunks[0] = 0xdfd715fdd1ee730

- if result < converted number return 1 (else return 0)

### Aggregate ###

- take the result of the multiplication and add it with result of addition

    0x13ad2 + 1 = 0x13ad3

- store the result in chunks[1]

    chunks[1] = 0x13ad3

- at the end of this round


    chunks[0]=0xdfd715fdd1ee730
    chunks[1]=0x13ad3

## Round 2 ##

- conversion to integer

    '7718821644187074559' => '0x6b1ec363aa5dffff'

- we now have 2 chunks (each step in mul and add)

### Multiplication ###

- mul previous chunk with div_base.base

    chunks[0] * div_base.base = 
    0xdfd715fdd1ee730 * 0x8ac7230489e80000 = 0x795810ab4b6bbbb (H) | 0x509d1a7a33800000 (L)

- add lower part with previous high part (step 0 -> cl=0x0)

    0x509d1a7a33800000 + 0x0 = 0x509d1a7a33800000

- store it in chunks

    chunks[0] = 0x509d1a7a33800000

- save current high part 0x795810ab4b6bbbb (cl=0x795810ab4b6bbbb)

- mul previous chunk with div_base.base

    chunks[1] * div_base.base = 
    0x13ad3 * 0x8ac7230489e80000 = 0xaaaa (H) | 0xa3156de43a380000 (L)

- add lower part with previous high part

    0xa3156de43a380000 + 0x795810ab4b6bbbb = 0xaaaaeeeeeeeebbbb

- store it in chunks

    chunks[1] = 0xaaaaeeeeeeeebbbb

- return high part -> 0xaaaa

### Addition ###

- add chunks[0] (lp) with converted number

    0x509d1a7a33800000 + 0x6b1ec363aa5dffff = 0xbbbbddddddddffff

- store result in chunks[0]
    
    chunks[0] = 0xbbbbddddddddffff

- if result < converted number return 1 (else return 0), 
here we return 0

- add chunks[1] (lp) with converted number

    0xaaaaeeeeeeeebbbb + 0x0 (previous step had returned 0x0)

- store result in chunks[1]

    chunks[1] = 0xaaaaeeeeeeeebbbb

### Aggregate ###

- take the result of the multiplication and add it with result of addition

    0xaaaa + 0x0 = 0xaaaa

- store result in chunks[2]

    chunks[2] = 0xaaaa

- at the end of this round


    chunks[0] = 0xbbbbddddddddffff
    chunks[1] = 0xaaaaeeeeeeeebbbb
    chunks[2] = 0xaaaa

