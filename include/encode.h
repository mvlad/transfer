#ifndef __ENCODE_H
#define __ENCODE_H


/**
 * @brief convert string to internal representation
 * @param num the number
 * @param str the input string
 * @param base_from the base of the input string
 * @retval 0 sucess
 * @retval -1 for failure
 */
extern int 
encode_str(num_t *num, const char *str, int base_from);

#endif /* __ENCODE_H */
