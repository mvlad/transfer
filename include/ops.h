#ifndef __OPS_H
#define __OPS_H

#if (defined(__i386__) || defined(__i486__) || defined(__i686__))
#error "i386 unsupported!"
#endif

/**
 * @addtogroup CORE
 * @{
 */ 

/**
 * @brief native multiplication of two 64-bit numbers
 * @param l result of the low part (rax)
 * @param h result of the high part (rdx)
 * @param u first number
 * @param v second number
 *
 * @verbatim
 * rdx  : rax
 * high   low
 * @endverbatim
 */
#define umul(h, l, u, v)                                                \
        __asm("mulq %3" : "=a" (l), "=d" (h)                            \
                        : "%0" ((unsigned long long) (u)),              \
                          "rm" ((unsigned long long) (v)))
/**
 * @brief native division of two 64-bit numbers
 * @param q stores quotient
 * @param r stores remainder
 * @param h input dividend high part
 * @param l input dividend low part
 * @param div divisor 
 *
 * @verbatim
 * rdx  : rax
 * high   low
 * @endverbatim
 */
#define udiv(q, r, h, l, div)                                           \
        __asm("divq %4" : "=a" (q), "=d" (r)                            \
                        : "0" ((unsigned long long) (l)),               \
                          "1" ((unsigned long long) (h)),               \
                         "rm" ((unsigned long long) (div)))

/**
 * @brief native count leading zeros
 * @param count stores the output 
 * @param x the input number
 *
 * @warning x must be > 0
 *
 */
#define clz(count, x) do {                                              \
        unsigned long long __tmp;                                       \
        __asm("bsrq %1,%0" : "=r" (__tmp)                               \
                           : "rm" ((unsigned long long) (x)));          \
        (count) = __tmp ^ 63;                                           \
} while (0)


/**
 * @brief native addition for two 64-bit numbers
 * @param ch the result high part
 * @param cl the result low part
 * @param ah first input number high part
 * @param al first input number low part
 * @param bh second input number high part
 * @param bl first input number low part
 *
 * ah|al + bh|bl = ch|cl
 */
#define add(ch, cl, ah, al, bh, bl)                                     \
          __asm__ ("addq %5,%q1\n"                                      \
                   "adcq %3,%q0"                                        \
                   : "=r" (ch), "=&r" (cl)                              \
                   : "0"   ((unsigned long long)(ah)),                  \
                     "rme" ((unsigned long long)(bh)),                  \
                     "%1"  ((unsigned long long)(al)),                  \
                     "rme" ((unsigned long long)(bl)))

/**
 * @brief add two 64-bit numbers -- C version
 * @deprecated
 */
#define add_c(sh, sl, ah, al, bh, bl)  do {                             \
                                                                        \
        unsigned long __x = (al) + (bl);                                \
                                                                        \
        if (__x < (al)) {                                               \
                (sh) = (ah) + (bh) + 1;                                 \
        } else {                                                        \
                (sh) = (ah) + (bh);                                     \
        }                                                               \
                                                                        \
        (sl) = __x;                                                     \
} while (0)

/**
 * @brief count leading zeros -- C version
 * @deprecated
 */
#define clz_c(count, x)   do {                                          \
        unsigned long y = (x);                                          \
        unsigned c;                                                     \
                                                                        \
        unsigned long mask = ((unsigned long) 0xff <<                   \
                        (CHUNK_BITS - 8));                              \
                                                                        \
        for (c = 0;  (y & mask) == 0; c += 8)                           \
                y <<= 8;                                                \
                                                                        \
        for (; (y & CHUNK_HIGHBIT) == 0; c++)                           \
                y <<= 1;                                                \
                                                                        \
        (count) = c;                                                    \
} while (0)

struct div_base;

/**
 * @brief add to chunk the contents of val
 * @param dst a pointer to chunks
 * @param n how many chunks
 * @param val the value to add
 * @return 1 if addition overflowed
 * @return 0 if addition didn't overflowed 
 *
 * @warning val will be modifed on the next round of addition
 */
extern unsigned long
chunk_add(unsigned long *dst,
        long int n, unsigned long val);

/**
 * @brief multiply chunks with the contents of val
 * @param dst a pointer to chunks
 * @param n how many chunks
 * @param val the value to multiply with
 * @retval the highest significant part
 */
extern unsigned long
chunk_mul(unsigned long *dst,
        long int n, unsigned long val);

/**
 * @brief left shift from src chunk to dst chunk with shift bits
 * @param dst destination chunk
 * @param src source chunk
 * @param n how many chunks to shift
 * @param shift how many bits to shift
 * @retval the 
 */
extern unsigned long 
chunk_lshift(unsigned long *dst, const unsigned long *src,
        long int n, unsigned int shift);

/**
 * @brief divide n chunks with divisor div
 * @param chunk a pointer to chunks
 * @param n how many chunks to divide
 * @param high the high part required for division
 * @param div the divisor
 * @retval returns the remainder when the we've gone over 
 * all chunks (n)
 *
 * @note high will be used for the first round, 
 * so that in next round(s) will be replaced by the contents
 * of the remainder of the previous round
 * @note the quotients are stored in the chunks
 * @note the lower part is retrived from the chunk
 */
extern unsigned long
chunk_div(unsigned long *chunk, long int n,
        unsigned long high, unsigned long div);

/**
 * @brief divide n chunks with the base from info pointer
 * @param chunk a pointer to chunks
 * @param n a pointer to the number of chunks
 * @param info a pointer to a div_base structure
 * @retval the remainder from dividing n chunks with base from
 * info
 *
 * @note n will be modifed 
 */
unsigned long
chunk_div_naive(unsigned long *chunk, long int *n, 
        const struct div_base *info);

/**
 * @brief divide the input value w by base until quotient reaches zero
 * @param w the number
 * @param base the divisior
 * @retval the number of divisions done
 */
extern int
div_naive(unsigned long w, int base);

/**
 * @}
 */

#endif /* __OPS_H */
