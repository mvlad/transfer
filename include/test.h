#ifndef __TEST_H
#define __TEST_H

#define ok1_len(__asn, __test)                          \
        size_t l1 = strlen(__asn);                      \
        size_t l2 = strlen(__test);                     \
                                                        \
        eprintf("Testing sizes\n");                     \
        if (l1 == l2) {                                 \
                eprintf("Len OK!\n");                   \
        } else {                                        \
                eprintf("Len mismatch\n");              \
        }                                               
                

#define ok1(__ans, __test)                              \
        if (eq(__ans, __test)) {                        \
                eprintf("OK!\n");                       \
        } else {                                        \
                eprintf("FAILED!\n");                   \
                eprintf("Got:      %s\n", __ans);       \
                eprintf("Expected: %s\n", __test);      \
                exit(EXIT_FAILURE);                     \
        }

#endif /* __TEST_H */
