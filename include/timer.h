#ifndef __TIMER_H
#define __TIMER_H

/**
 * @addtogroup UTIL
 * @{
 */

#include "log.h"
#include <time.h>

#define MS              (1000 * 1000)
#define NANOSEC         (1000 * MS)

/** compute difference between two timespecs */
#define tv_diff(t2, t1) \
        (((t2.tv_sec * NANOSEC) + t2.tv_nsec) -\
         ((t1.tv_sec * NANOSEC) + t1.tv_nsec))


/**
 * @brief storage for timer
 */
struct timer {
        struct timespec ts_end;         /**< when it ended */
        struct timespec ts_start;       /**< when it started */

        /**< how long it took */
        struct {
                unsigned long ms;       /**< miliseconds */
                double secs;            /**< seconds */
                double mins;            /**< minutes */
        } elapsed;
};

/**
 * @brief retrive current time in tv
 * @param tv a pointer to a timespec
 */
static __inline
void get_current_ts(struct timespec *tv)
{
        if (clock_gettime(CLOCK_MONOTONIC_RAW, tv) == -1) {
                dprintf("Could not get time of day\n");
                exit(EXIT_FAILURE);
        }
}

/**
 * @brief compute wallclock
 */
static __inline void 
__wallclock(struct timer *tm)
{
        struct timespec t1 = tm->ts_start;
        struct timespec t2 = tm->ts_end;

        tm->elapsed.ms = tv_diff(t2, t1) / MS;
        tm->elapsed.secs = (double) tm->elapsed.ms / 1000;
        tm->elapsed.mins = (double) tm->elapsed.secs / 60;
}

/**
 * @brief add two timers
 */
static __inline void
add_tm_to_tm(struct timer *tm1, struct timer *tm2)
{
        tm1->elapsed.ms += tm2->elapsed.ms;
        tm1->elapsed.secs += tm2->elapsed.secs;
        tm1->elapsed.mins += tm2->elapsed.mins;
}

static __inline void 
__show_wallclock(const char *m, struct timer *tm)
{
        eprintf("%s: %lums (%fs, %fm)\n", 
                m, tm->elapsed.ms, tm->elapsed.secs, tm->elapsed.mins);
}

/**
 * @brief statically allocate a timer for usage with
 * \c START|STOP_TIMER macros
 */
#define INIT_TIMER(tm)  \
        struct timer tm = { { 0 }, { 0 }, { 0 } }


/**
 * @brief start the timer by retrieving current time
 */
#define START_TIMER(timer)                      \
        get_current_ts(&(timer.ts_start));      

/**
 * @brief stop the timer 
 */
#define STOP_TIMER(timer)                       \
        get_current_ts(&(timer.ts_end));        \
        __wallclock(&timer)

/**
 * @brief display the time difference stored in a timer structure
 * @param mess a string passed as a prefix message for display
 * @param tm a \c timer structure created with \c INIT_TIMER
 */
#define SHOW_WALLCLOCK(mess, tm)        \
        __show_wallclock(mess, &tm)

/**
 * @brief add two timers
 */
#define ADD_TM_TO_TM(tm1, tm2)        \
        add_tm_to_tm(&tm1, &tm2)

#endif /* __TIMER_H */
