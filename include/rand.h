#ifndef __RAND_H
#define __RAND_H

/**
 * @addtogroup UTIL
 * @{
 */

/**
 * @brief randomly generate a digit string of size len
 * @param len the maximum length to generate
 * @retval a pointer to the beginning of the string
 *
 * @note caller is responsible of freeing free it
 */
extern 
char *gen_digit_str(long int len);

/**
 * @brief radomly generate an alphanumber string of size len
 * @param len the maximum length to generate
 * @param b in which base to generate the string
 * @retval a pointer to the beginning of the string
 *
 * @note caller is responsible of freeing free it
 */
extern 
char *gen_alphanum_str(long int len, int b);

/**
 * @}
 */

#endif /* __RAND_H */
