#ifndef __XUTIL_H
#define __XUTIL_H

/**
 * @defgroup UTIL UTIL
 * @addtogroup UTIL
 * @{
 */

/** @warning n == 0 fails */
#define POW_OF_2(n)     (((n) & ((n) - 1)) == 0) 
#define ABS(x)          ((x) >= 0 ? (x) : -(x))

#define MIN(a, b)       ((a) <= (b) ? (a) : (b))
#define MAX(a, b)       ((a) >= (b) ? (a) : (b))

extern void 
*xmalloc(unsigned int size);

extern void 
*xcalloc(unsigned int numb, unsigned int size);

extern void 
*xrealloc(void *old, unsigned int size);

extern void 
xfree(void *ptr);

/* I/O */
extern size_t 
xread(int fd, void *buf, size_t size);

extern size_t 
xwrite(int fd, const char *buf, size_t size);

/* string */
extern void 
xskipwhitespace(const char *str);

extern size_t 
xstrlcpy(char *dst, const char *str, size_t len);

extern char 
*xstrcpy(char *dst, const char *str);

extern int 
eq(const char *a, const char *b);

extern int 
neq(const char *a, const char *b);

/**
 * @}
 */

#endif /* __XUTIL_H */
