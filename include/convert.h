#ifndef __CONVERT_H
#define __CONVERT_H

/**
 * @brief store the contents of the remainders in str (naive method)
 * @param str the output string
 * @param base the base to convert
 * @param chunks internal data storing the big number
 * @param nr number of chunks
 * @retval the length of the string
 *
 * @note this method can be passed to the generic @link convert @endlink
 */
extern size_t
__convert_str(unsigned char *str, int base,
                unsigned long *chunks, long int nr);

/** 
 * @brief generic conversion function
 *
 * - \c __convert_str()     -- naive method
 */
typedef size_t (*convert_func)(unsigned char *str, int b, 
                unsigned long *chunks, long int nr);

/**
 * @brief generic function for conversion
 * @param num a big number
 * @param sp where store the converted number
 * @param base the base to convert to
 * @param cb generic conversion function
 * @return the beginning of sp, caller is responsible free'ing it
 *
 * @note if sp is NULL we allocate it
 * @note sp will point at the end of the string
 */
extern
char *convert(num_t *num, char *sp, int base, convert_func cb);

/**
 * @brief determine the length of the converted string using
 * naive divisions
 * @param num the input 
 * @param base what base to convert to
 * @retval how many remainders, and therefore how big is the ouput string
 */
extern size_t 
sz(const num_t *num, int base);

/**
 * @brief determine the length of the converted string using approximation(s)
 * @param num the input 
 * @param base what base to convert to
 * @retval how many remainders, and therefore how big is the ouput string
 */
extern size_t 
sz_approx(const num_t *num, int base);

/**
 * @brief naive method for converting a string
 * @param num a big number
 * @param sp a buffer where to store the result
 * @param base the base to convert to
 *
 * @note sp is allocated if NULL
 * @note sp will point at the end of the string
 */
extern
char *convert_str(num_t *num, char *sp, int base);

#endif /* __CONVERT_H */
