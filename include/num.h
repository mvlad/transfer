#ifndef __NUM_H
#define __NUM_H

#if (defined(__i386__) || defined(__i486__) || defined(__i686__))
#error "i386 not supported!"
#endif

/**
 * @defgroup CORE CORE
 * @addtogroup CORE
 * @{
 */

#include "ops.h"

/*
 * signed 
 *      from 0x8000000000000000 (-9223372036854775808)
 *      to   0x7fffffffffffffff (9223372036854775807)
 *
 * unsigned
 *      from 0xUL (0) to 0xffffffffffffffff (18446744073709551615)
 *
 *      xxxx 0xffffffffffffffff (18446744073709551615)  -- CHUNK_T_MAX
 *      .... ..................
 *      .... 0x8000000000000000 (9223372036854775808)   -- CHUNK_HIGHBIT
 *      .... 0x7fffffffffffffff (9223372036854775807)   -- CHUNK_T_MAX >> 1
 *      .... 0x0000000100000000 (4294967296)            -- HCHUNK_BIT
 *      .... 0x00000000ffffffff (4294967295)            -- LCHUNK_MASK (unsigned 32 MAX)
 *      .... ..................
 *      xxxx 0x0000000000000000 (0000000000000000000)
 */

/** how big is a chunk */
#define CHUNK_BITS      64

/** how much can be stored in a unsigned long 0xffffffffffffffff */
#define CHUNK_T_MAX     (~(unsigned long) 0)    

/** 0x0000000100000000 */
#define HCHUNK_BIT      ((unsigned long) 1 << (CHUNK_BITS / 2))
/** 0x00000000ffffffff */
#define LCHUNK_MASK     (HCHUNK_BIT - 1)
#define HALF_BITS       (CHUNK_BITS / 2)

#define INIT_CHUNK      8

#define __USHRT_MAX     (0 + (unsigned short)~0)
#define __UINT_MAX      (~(unsigned) 0)
#define __ULONG_MAX     (~(unsigned long) 0)

#define USHRT_HIGHBIT   (__USHRT_MAX ^ ((unsigned short) __USHRT_MAX >> 1))
#define UINT_HIGHBIT    (__UINT_MAX  ^ ((unsigned) __UINT_MAX >> 1))
#define ULONG_HIGHBIT   (__ULONG_MAX ^ ((unsigned long) __ULONG_MAX >> 1))

/** 0x8000000000000000 */
#define CHUNK_HIGHBIT   (CHUNK_T_MAX ^ (CHUNK_T_MAX >> 1))

#define __LONG_MIN      ((long) ULONG_HIGHBIT)
#define __LONG_MAX      (-(__LONG_MIN + 1))

#define SIZE_T_MIN      __LONG_MIN
#define SIZE_T_MAX      __LONG_MAX


/** flag set by apps */
extern unsigned int run_flag;

enum { EMPTY = 1, VERB, DEBUG, TIME, METH_NAIVE };

/** 
 * @brief potential run flags 
 * 
 */
enum {
        IS_VERB         = 1 << VERB,
        IS_DEBUG        = 1 << DEBUG,
        IS_TIME         = 1 << TIME,
        IS_METH_NAIVE   = 1 << METH_NAIVE,
};

#define FLAG_SET(flg)           ((1 << flg) & run_flag)

/**
 * @brief internal data representation of number
 */
typedef struct num {

        int base_from;          /**< what base is originally represented */
        size_t in_len;          /**< size of the input number */
        size_t out_len;         /**< size of converted number */

        int alloced;            /**< how many chunks have been alloc'ed */
        int size;               /**< how many chunks are used */

        unsigned long *chunk;    /**< number encoded into chunks */
} num_t;

#define NUM_GET_BASE(n)         (n)->base_from
#define NUM_GET_IN(n)           (n)->in_len
#define NUM_GET_OUT(n)          (n)->out_len

#define NUM_GET_ALLOCED(n)      (n)->alloced
#define NUM_GET_SIZE(n)         (n)->size
#define NUM_GET_CHUNKS(n)       (n)->chunk


/**
 * @brief storage information for the divisor
 */
struct div_base {

        /** largest power of the base */
        unsigned long int base;

        /** exponent how much we take of the input string */
        unsigned int exp;

        /** shift in case the divisor needs to be normalized */
        unsigned int shift;
};

enum {
        POW_0 = 1,
        POW_1 = 1 << 1,
        POW_2 = 1 << 2,
        POW_3 = 1 << 3,
        POW_4 = 1 << 4,
        POW_5 = 1 << 5,
        POW_6 = 1 << 6,
        POW_7 = 1 << 7,
        POW_8 = 1 << 8
};


/**
 * @brief allocate a new number
 */
extern void num_init(num_t *num);

/**
 * @brief purge/free number and its chunks
 */
extern void num_free(num_t *num);

/**
 * @brief print all the chunks of a number
 */
extern void num_print_chunks(num_t *num);

/**
 * @brief allocate chunks
 * @param size the number of chunks to allocate
 * @retval a pointer to the beginning of the first chunk
 */
extern void
*alloc_chunks(long int size);

/**
 * @brief allocate size chunks for number n
 * @param n the big num
 * @param size the number of chunks to allocate
 * @retval a pointer to the beginning of the first chunk
 */
extern unsigned long
*num_alloc_chunks(num_t *n, long int size);

/**
 * @brief re-allocate chunks
 * @param n the number
 * @param size how many chunk we want to realloc
 * @retval a pointer to the beginning of the new chunk
 *
 * @deprecated
 */
extern unsigned long
*num_realloc_chunks(num_t *n, long int size);

/** 
 * @brief realloc wrapper for an num_t if it has 
 * less than needed chunks
 *
 * @deprecated
 */
static __inline void
*__realloc(num_t *num, long int size)
{
        if (size > num->alloced)
                return num_realloc_chunks(num, size);
        else
                return num->chunk;
}

/**
 * @brief copy chunks
 * @param d destination
 * @param s source
 * @param n amount of chunks to copy
 */
extern void copy_chunks(unsigned long *d, 
        const unsigned long *s, long int n);

/**
 * @brief determine the power of number b in case it is a power of two
 * @return the exponent
 */
static __inline unsigned int
bits_pow_of_2(unsigned int b)
{
        static unsigned int lkup[] = {
                [POW_1] = 1, /* 2       */
                [POW_2] = 2, /* 4       */
                [POW_3] = 3, /* 8       */
                [POW_4] = 4, /* 16      */
                [POW_5] = 5, /* 32      */
        };
        return (b > 0) ? lkup[b] : 0;
}

/**
 * @brief compute mantisa and exponent for given base
 * @param div a pointer to a div_base
 * @param base the number given
 *
 * @note populates the contents of div
 */
static __inline void 
get_div_base(struct div_base *div, unsigned long base)
{
        unsigned long p;
        unsigned exp;
        int shift;

        p = base;
        for (exp = 1; p <= CHUNK_T_MAX / base; exp++) {
                p *= base;
        }

        /* find out if the base has leading zeros */
        clz(shift, p);

        div->exp       = exp;
        div->base      = p;
        div->shift     = shift;
}
#endif  /* __NUM_H */

/**
 * @}
 */

