#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

/**
 * @addtogroup CORE
 * @{
 */

#include "num.h"
#include "log.h"
#include "xutil.h"

#include "ops.h"
#include "encode.h"

/**
 * @brief determine the number of trailing zeros 
 * @param ck the chunk
 * @param n the size of the input chunk
 * @retval the number of zeros
 */
static long int
norm_size(const unsigned long *ck, long int n)
{
        for (; n > 0 && ck[n - 1] == 0; n--)
                ; /* do nothing */

        return n;
}

/**
 * @brief encode from byte array to internal chunks when 
 * the base is a power of two
 * @param chunk a pointer to allocated chunks where to store 
 * the encoded string
 * @param d the input string
 * @param sn the length of the input string
 * @param bits how many bits we need to shift
 * @return how many chunks have been created
 */
static long int
__encode_pow_of_2(unsigned long *chunk, const unsigned char *d, 
        size_t sn, unsigned bits)
{
        long int n;
        size_t j;
        unsigned shift;

        for (j = sn, n = 0, shift = 0; j-- > 0; ) {
                if (shift == 0) {
                        /* the most significant part */
                        chunk[n++] = d[j];
                        shift += bits;
                } else {
                        unsigned long t1;

                        /* shift each char */
                        t1 = (unsigned long) d[j] << shift;

                        /* add it with previous value */
                        chunk[n - 1] |= t1;

                        /* increase the number to shift for next round */
                        shift += bits;

                        if (shift >= CHUNK_BITS) {
                                
                                /* adjust for next round */
                                shift -= CHUNK_BITS;

                                if (shift > 0) {
                                        unsigned long t2;

                                        t2 = (unsigned long) d[j] >> (bits - shift);
                                        chunk[n++] = t2;
                                }
                        }
                }
        }

        n = norm_size(chunk, n);
        return n;
}


/**
 * @brief encode from byte array to input chunks when base 
 * is not a power of 2
 * @param chunks where to store the converted string
 * @param d the input string
 * @param sn the size of the input string
 * @param base_from from which base
 * @param info div_base retrived in previous step
 * @return how many chunks have been created
 */
static long int
__encode(unsigned long *chunks, const unsigned char *d, size_t sn, 
        unsigned long base_from, const struct div_base *info)
{
        long int n;
        unsigned long ul;

        unsigned int k;
        size_t j = 0;

        /* how many bytes we take in the first run */
        k = (sn - 1) % info->exp + 1;
        dprintf("Initial split will take %u bytes\n", k);

        ul = d[j++];
        while (--k) {
                ul = ul * base_from + d[j++];
        }

        /* store it as the least significant part */
        chunks[0] = ul;

        eprintf("Converting other chunks to integers\n");

        /* 
         * Split the rest of the byte array:
         *
         * - multiply (first time by the least part we computed previously) each
         *   chunk with the larget value that can be stored in a unsigned long 
         * - add the contents of the converted integer to each chunk
         * - store it in the chunk as the most significant chunk
         */
        for (n = (ul > 0); j < sn;) {
                unsigned long lp;

                ul = d[j++];
                for (k = 1; k < info->exp; k++) {
                        ul = ul * base_from + d[j++];
                }

                /* multiply each chunk with info->base 
                 * + return the most significant part */
                lp = chunk_mul(chunks, n, info->base);

                /* add each chunk with the integer value 
                 * + returns carry */
                lp += chunk_add(chunks, n, ul);

                if (lp > 0) {
                        chunks[n++] = lp;
                }
        }

        assert(j == sn);
        return n;
}

/**
 * @brief try to figure out which base the input string is,
 * when the base_from supplied in encode() is 0
 * @param str the string
 * @retval the base which the string is
 */
static int
__get_base(const char *str)
{
        int base_from = 0;

        if (*str == '0') {
                str++;
                switch (*str) {
                case 'x':
                        base_from = 16; str++;
                        break;
                case 'b':
                        base_from = 2; str++;
                        break;
                default:
                        base_from = 8;
                        break;

                }
        } else { /* default case */
                base_from = 10;
        }

        return base_from;
}

/**
 * @brief converts the input string str to a byte-array in d
 * @param num the big number
 * @param d the result
 * @param str the input string
 * @param base_from the base to which to covert to
 * @retval the length of the converted string
 */
static size_t
to_bytes(num_t *num, unsigned char *d, const char *str, int base_from)
{
        size_t sn;

        eprintf("Converting input to a byte-array\n");

        for (sn = 0; *str; str++) {
                unsigned int digit;

                if (*str >= '0' && *str <= '9') {
                        digit = *str - '0';
                } else if (*str >= 'a' && *str <= 'z') {
                        digit = *str - 'a' + 10;
                } else if (*str >= 'A' && *str <= 'Z') {
                        digit = *str - 'A' + 10;
                } else {
                        dprintf("Failed to determine digit!\n");
                        digit = base_from; /* fail */
                }

                if (digit >= (unsigned) base_from) {
                        xfree(d);
                        num->size = 0;
                        return 0;
                }
                d[sn++] = digit;
        }
        eprintf("Conversion to byte-array done!\n");
        return sn;
}

int
encode_str(num_t *num, const char *str, int base_from)
{
        long int n, alloc;

        unsigned long *chunks;
        unsigned char *d;

        size_t sn;
        int sign = 0;

        assert((base_from >= 2 && base_from <= 36));

        xskipwhitespace(str);

        if (*str == '-') {
                sign = 1; 
                str++;
        }

        /* try to figure base */
        if (base_from == 0) {
                base_from = __get_base(str);
        }

        dprintf("Input base to convert from %d\n", base_from);

        sn = strlen(str);
        d = xmalloc(sn + (sn == 0));

        /* convert to a byte-array */
        sn = to_bytes(num, d, str, base_from);
        if (!sn) {
                dprintf("Failed to convert to a byte array\n");
                return -1;
        }

        dprintf("Converted string to byte-array (length %ld)\n", sn);

        if (POW_OF_2(base_from)) {

                unsigned bits = bits_pow_of_2(base_from);

                /* determine how many chunks we need to alloc */
                alloc = (sn * bits + CHUNK_BITS - 1) / CHUNK_BITS;

                chunks = num_alloc_chunks(num, alloc);

                dprintf("Allocated %ld chunks\n", alloc);
                n = __encode_pow_of_2(chunks, d, sn, bits);
        } else {

                struct div_base div;
                get_div_base(&div, base_from);

                dprintf("div.base=0x%lx, div.exp=0x%x, div.shift=0x%x\n",
                                div.base, div.exp, div.shift);

                /* determine how many chunks we need to alloc */
                alloc = (sn + div.exp - 1) / div.exp;

                chunks = num_alloc_chunks(num, alloc);

                dprintf("Allocated %ld chunks\n", alloc);
                n = __encode(chunks, d, sn, base_from, &div);

#if ENABLE_DEBUG
                long int c;
                for (c = 0; c < n; c++) {
                        dprintf("chunk[%ld]=0x%lx\n", c, chunks[c]);
                }
#endif
        }

        assert(n <= alloc);
        xfree(d);

        if (sign) {
                num->size = -n;
        } else {
                num->size = n;
        }

        num->in_len = sn;
        num->base_from = base_from;

        return 0;
}
/**
 * @}
 */
