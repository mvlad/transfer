#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "num.h"
#include "log.h"
#include "xutil.h"

#include "ops.h"

unsigned long
chunk_add(unsigned long *chunk,
        long int n, unsigned long val)
{
        long int i;
        assert(n > 0);

        for (i = 0; i < n; i++) {

                chunk[i] = chunk[i] + val;

                /* Carry out for next round */
                if (chunk[i] < val) {
                        val = 1;
                } else {
                        val = 0;
                }

        }
        return val;
}

unsigned long 
chunk_mul(unsigned long *chunk,
        long int n, unsigned long val)
{
        long int i;
        unsigned long ul;
        unsigned long hp, lp;

        assert(n >= 1);

        unsigned long cl = 0;
        for (i = 0; i < n; i++) {

                ul = chunk[i];
                umul(hp, lp, ul, val);

                lp += cl;
                chunk[i] = lp;

                if (lp < cl) {
                        cl = 1 + hp;
                } else {
                        cl = hp;
                }
        }

        return cl;
}

unsigned long 
chunk_lshift(unsigned long *dst, const unsigned long *src,
        long int n, unsigned int shift)
{
        unsigned long high, low;
        unsigned int ishift;
        unsigned long ret;

        assert(n >= 1);
        assert(shift >= 1);
        assert(shift < CHUNK_BITS);

        src += n;
        dst += n;

        ishift = CHUNK_BITS - shift;

        low = *--src;

        ret     = low >> ishift;
        high    = low << shift;

        while (--n) {
                low = *--src;
                *--dst = high | (low >> ishift);
                high = (low << shift);
        }
        *--dst = high;

        return ret;
}

__inline unsigned long
chunk_div(unsigned long *chunk, long int n, 
        unsigned long high, unsigned long div)
{
        unsigned long r, low;

        while (n > 0) { 

                low = chunk[--n];

                /* store the quotients in the same position */
                udiv(chunk[n], r, high, low, div);

                /* for next round use the remainder as the higher part */
                high = r;
        }

        return high;
}

/**
 * @brief adjust the divisor and the high part
 * @param chunk a pointer to the chunks 
 * @param n the number of chunks
 * @param div_base a div_base pointer
 * @param __high a pointer of the high part
 * @param __div a pointer of the divisor
 *
 * @note __high gets modified with computed value
 * @note __div gets modified with computed value
 */
static __inline unsigned long
adjust_div(unsigned long *chunk, long int n,
        const struct div_base *div_base, 
        unsigned long *__high, unsigned long *__div)
{
        unsigned long low;
        unsigned long q, r;

        unsigned long high = *__high;
        unsigned long div = *__div;

        dprintf("Adjusting the divisor\n");

        div = div_base->base << div_base->shift;

        /* we need to adjust the high|low part */
        low = (high << div_base->shift);

        /* left-shift n chunks with shift and return the last lower part */
        if (n) {
                low |= chunk_lshift(chunk, chunk, n, div_base->shift);
        }
        high >>= (CHUNK_BITS - div_base->shift);

        dprintf("H=0x%lx, L=0x%lx, DIV=0x%lx\n", high, low, div);

        udiv(q, r, high, low, div);

        dprintf("Q=0x%lx, R=0x%lx\n", q, r);

        /* return new high part and divisor */
        *__high = r;
        *__div = div;

        /* save the quotient */
        return q;
}

__inline unsigned long
chunk_div_naive(unsigned long *chunk, long int *nr, 
                const struct div_base *div_base)
{
        unsigned long high, rr, q;
        unsigned long div = div_base->base;

        int tshift = 0;

        /* how many chunks */
        long int n = *nr;

        /* source -- the high part */
        high = chunk[--n];

        /* 
         * determine if the division would result in q being zero
         */
        if (high >= div) {
                if (div_base->shift) {
                        /* we adjust the divisor and the high part */
                        q = adjust_div(chunk, n, div_base, &high, &div);
                        tshift = 1;
                } else {
                        /* we'll need to do another round */
                        q = 1;
                        high -= (-q) & div;
                }
        } else {
                /* 
                 * we only go onto the next round if the quotient is zero,
                 * otherwise perform another round of divisions
                 */
                q = 0; 

                /* reset divisor to old value */
                div = div_base->base;
                
                /* go onto the next round */
                (*nr)--;
                high -= (-q) & div;
        }

        /* dst, store quotient here */
        chunk[n] = q;

        /* 
         * now, the high part has been determined, divide
         * all chunks with (potentially) new divisor
         */
        rr = chunk_div(chunk, n, high, div);

        if (tshift) {
                rr >>= div_base->shift;
        }

        return rr;
}

__inline int
div_naive(unsigned long int ul, int base)
{
        size_t i = 0;

        unsigned long int r, uh = 0x0;
        unsigned long int q;

        while (ul > 0) {
                udiv(q, r, uh, ul, base);
                /* use quotient for lower part */
                ul = q; i++;

        }
        return i;
}
