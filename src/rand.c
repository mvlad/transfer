#include <stdio.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include "num.h"
#include "log.h"
#include "timer.h"
#include "xutil.h"
#include "rand.h"

static const char alphanum[] = "0123456789abcdefghijklmnopqrstuvwxyz";
static const char digits[] = "0123456789";

/**
 * @brief generate a random char using rand() 
 * @param b the maximum value the return value can have
 * @retval a unsigned char 
 *
 * @note we use ```rand(3)``` routine seeded with
 * tv_nsec from a timespec. The b paramter limits
 * what the rand() returns.
 */
static unsigned char
get_rand(int b)
{
        unsigned seed;
        struct timespec ts;
        int len_str = 0;

        if (!b)
                len_str = strlen(digits);

        get_current_ts(&ts);

        seed = (unsigned) ts.tv_nsec;
        srand(seed);
        if (!b)
                return (rand() % len_str);
        else
                return (rand() % b);
}

char 
*gen_digit_str(long int len)
{
        char *res;
        int r; 

        res = xcalloc(len + 1, sizeof(char));
        memset(res, 0, len + 1);

        for (r = 0; r < len; r++) {
                int c;

                /* do not allow 0 in front */
                if (r == 0) {
                        while ((c = get_rand(0)) == 0)
                                ; /* nothing */
                } else {
                        c = get_rand(0);
                }

                res[r] = digits[c];
        }

        return res;
}

char
*gen_alphanum_str(long int len, int b)
{
        char *res;
        int i; 

        res = xcalloc(len + 1, sizeof(char));

        for (i = 0; i < len; i++) {
                unsigned char c;

                if (i == 0) {
                        while ((c = get_rand(b)) == 0)
                                ; /* nada */
                } else {
                        c = get_rand(b);
                }

                res[i] = alphanum[c];
        }

        return res;
}

