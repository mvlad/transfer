#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <unistd.h>
#include <errno.h>

#include "num.h"
#include "log.h"
#include "xutil.h"

void 
*xmalloc(unsigned int size)
{
        void *ptr = malloc(size);

        if (size && !ptr) {
                fprintf(stderr, "memory allocation failed\n");
                exit(EXIT_FAILURE);
        }
        memset(ptr, 0, size);
        return ptr;
}

void 
*xcalloc(unsigned int numb, unsigned int size)
{
        void *ptr = calloc(numb, size);

        if (numb && size && !ptr) {
                fprintf(stderr, "memory callocation failed\n");
                exit(EXIT_FAILURE);
        }
        return ptr;
}

void
*xrealloc(void *ptr, unsigned int size)
{
        void *nptr; 

        nptr = realloc(ptr, size);

        if (!nptr) {
                fprintf(stderr, "memory re-allocation failed\n");
                exit(EXIT_FAILURE);
        }
        return nptr;
}

void 
xfree(void *ptr)
{
        if (ptr) {
                free(ptr);
        }
        ptr = NULL;
}


size_t 
xread(int fd, void *buf, size_t size)
{
        ssize_t total = 0;

        while (size) {
                ssize_t ret;
                ret = read(fd, buf, size);

                if (ret < 0 && errno == EINTR)
                        continue;

                /* no more to read */
                if (ret <= 0)
                        break;

                buf = (char *) buf + ret;
                size -= ret;
                total += ret;
        }

        return total;
}

size_t 
xwrite(int fd, const char *buf, size_t size)
{
        ssize_t total = 0;

        while (size) {
                ssize_t ret;
                ret = write(fd, buf, size);

                if (ret < 0 && errno == EINTR)
                        continue;

                /* no more to write */
                if (ret <= 0)
                        break;

                buf = (const char *) buf + ret;
                size -= ret;
                total += ret;
        }

        return total;
}

char 
*xstrcpy(char *dst, const char *src)
{
        char *ret;
        ret = dst;

        while ((*dst++ = *src++) != '\0')
                /* do nothing */;
        return ret;
}

size_t
xstrlcpy(char *dst, const char *src, size_t size)
{
        char *dst_in;

        dst_in = dst;
        if (size > 0) {
                while (--size > 0 && *src != '\0')
                        *dst++ = *src++;
                *dst = '\0';
        }
        return dst - dst_in;
}

int 
eq(const char *a, const char *b)
{
        if (!strcmp(a, b)) {
                return 1;
        }
        return 0;
}

int 
neq(const char *a, const char *b)
{
        if (!eq(a, b)) {
                return 1;
        }
        return 0;
}

void
xskipwhitespace(const char *str)
{
        while (*str == ' ' || *str == '\t')
                str++;
}

#if 0
size_t
xstrlen(const char *str)
{
        int n;

        if (str == NULL)
                return 0;

        const char *s = str;
        for (n = 0; *s != '\0'; s++)
                n++;
        return n;
}

int 
xstrcmp(const char *a, const char *b)
{
        const char *s1, *s2;

        s1 = a; s2 = b;

        while (*s1 && *s1 == *s2) {
                s1++; s2++;
        }
        return (int) ((unsigned char) *s1 - (unsigned char) *s2);
}

char 
*xstrcat(char *dst, const char *src)
{
        int l;

        l = xstrlen(dst);
        xstrcpy(dst + l, src);

        return dst;
}

void
*xmemset(void *v, int c, size_t len)
{
        char *p = v;
        int m = len;

        while (--m >= 0)
                *p++ = c;
        return v;

}

void 
*xmemmove(void *dst, const void *src, size_t n)
{
        const char *s;
        char *d;

        s = src;
        d = dst;

        if (s < d && s + n > d) {
                s += n;
                d += n;
                while (n-- > 0)
                        *--d = *--s;
        } else {
                while (n-- > 0) 
                        *d++ = *s++;
        }

        return dst;
}


void 
*xmemcpy(void *dst, void *src, size_t n)
{
        return xmemmove(dst, src, n);
}

unsigned long int
xstroul(const char *str, char **end, int base)
{
        unsigned long int val = 0UL;
        int neg;

        xskipwhitespace(str);

        if (*str == '+') {
                str++;
        } else if (*str == '-') {
                neg = 1; str++;
        }

        if ((base == 0 || base == 16) &&
            (str[0] == '0' && str[1] == 'x')) {
                str += 2; 
                base = 16;
        } else if (base == 0 && str[0] == '0') {
                str++;
                base = 8;
        } else if (base == 0) {
                base = 10;
        }


        while (str && *str) {
                int digit;

                if (*str >= '0' && *str <= '9') {
                        digit = *str - '0';
                } else if (*str >= 'a' && *str <= 'z') {
                        digit = *str - 'a' + 10;
                } else if (*str >= 'A' && *str <= 'Z') {
                        digit = *str - 'A' + 10;
                } else {
                        break;
                }

                if (digit >= base) {
                        break;
                }

                val = (val * base) + digit;
                str++;
        }


        if (end) {
                *end = (char *) str;
        }

        return (neg ? -val : val);
}
#endif
