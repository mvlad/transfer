#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/**
 * @addtogroup CORE
 * @{
 */

#include "num.h"
#include "log.h"
#include "xutil.h"

#include "ops.h"
#include "convert.h"

const char *digits = "0123456789abcdefghijklmnopqrstuvwxyz";

/**
 * @brief determine how many bits val has
 * @param val the input number
 * @retval the biggest value stored in a chunk
 * minus the leading zeros
 */
static unsigned long
bits_in_base_2(unsigned long val)
{
        unsigned shift;

        /* clz would fail otherwise */
        assert(val > 0);

        clz(shift, val);

        dprintf("clz(0x%lx)=%u (0x%x) => bits_in_base_2=0x%x\n", 
                        val, shift, shift, CHUNK_BITS - shift);
        return CHUNK_BITS - shift;
}

static __inline void
__reverse_order(unsigned char *str, size_t max)
{
        size_t i;

        for (i = 0; (2 * i) + 1 < max; i++) {
                unsigned char t = str[i];
                str[i] = str[max - i - 1];
                str[max - i - 1] = t;
        }
}

/**
 * @brief get the number of bits if base is a power of two
 * @param base the base to convert to
 * @param bits the input number of bits
 * @return 0 if the base is not a power of two
 * @return the number of bits based on the base size
 */
static __inline int
get_bits_pow_of_2(int base, unsigned long bits)
{
        switch (base) {
        case 2:         
                return bits;
        case 4:         
                return (bits + 1) / 2;
        case 8:         
                return (bits + 2) / 3;
        case 16:
                return (bits + 3) / 4;
        case 32:        
                return (bits + 4) / 5;
        default:
                dprintf("Base not a multiple of 2. "
                        "Calculating needed space...\n");
        }
        return 0;
}

/**
 * @brief get the number of bits out of the most significant chunk
 * @param num the big number
 * @retval the 
 */
static __inline unsigned long
get_chunk_bits(const num_t *num)
{
        unsigned long bits, bits_base2;
        unsigned long other_chunks;

        const unsigned long *chunk = num->chunk;

        /* get the # of bits of most significant chunk */
        bits_base2 = bits_in_base_2(chunk[num->size - 1]);

        /* get the # of bits for the other chunks */
        other_chunks = (num->size - 1) * CHUNK_BITS;

        bits = bits_base2 + other_chunks;

        return bits;
}

/**
 * @brief divide val by base until val reaches 0
 * @param str the string to write the remainders
 * @param base the base to divide
 * @param val the value itself
 * @retval how many remainders/divisions have been made
 *
 * @note the quotient is saved in val for next round
 * @note divq operation requires a 128-bit number so
 * for the higher part is always going to be 0, resulting
 * in a number having `0x00000000VALUE`
 * 
 */
static __inline size_t
__cnv_str(unsigned char *str, int base, unsigned long val)
{
        long int i = 0;

        for (i = 0; val > 0; i++) {

                /* 
                 * we specifically make the high part 0x0,
                 * basically dividing val (lower part) by base 
                 */
                unsigned long r, h = 0x0;

                /* 
                 * use val as the return value for the quotient,
                 * as well as input value for the next round as 
                 * the lower part
                 */
                udiv(val, r, h, val, base);

                /* save it */
                str[i] = r; 
        }
        return i;
}

/**
 * @brief reverse of __encode_pow_of_2, when conversion base (base_to) is a power of two
 * @param str the destination string
 * @param bits the number of bits
 * @param chunk a pointer to the encoded chunks
 * @param nr how many chunks we have
 * @retval length of the destination string
 */
static size_t
__convert_pow_of_2(unsigned char *str, unsigned bits, 
        const unsigned long *chunk, long int nr) 
{
        unsigned char mask;

        unsigned long bits_base2, other_chunks;

        size_t len, j;

        long int i;
        int shift;

        bits_base2 = bits_in_base_2(chunk[nr - 1]);
        other_chunks = (nr -1) * CHUNK_BITS;

        /* length of the output */
        len = (bits_base2 + other_chunks + bits - 1) / bits;

        mask = (1U << bits) - 1;

        for (i = 0, j = len, shift = 0; j-- > 0;) {

                unsigned char digit = chunk[i] >> shift;
                shift += bits;

                if (shift >= CHUNK_BITS) {
                        ++i;
                        if (nr > i) {
                                shift -= CHUNK_BITS;
                                digit |= chunk[i] << (bits - shift);
                        }
                }
                str[j] = digit & mask;
        }
        return len;
}

size_t
sz(const num_t *num, int base)
{
        int nr_chunks;
        unsigned long *tmp;

        unsigned long bits;
        unsigned long r;

        struct div_base info;

        long int pp;
        size_t digits = 0;

        nr_chunks = ABS(num->size);
        if (nr_chunks == 0) {
                return 1;
        }


        bits = get_chunk_bits(num);
        dprintf("Bits %lu (0x%lx)\n", bits, bits);
        dprintf("Base %d given to calculate size\n", base);
        if (get_bits_pow_of_2(base, bits)) {
                return bits;
        }

        tmp = alloc_chunks(nr_chunks);
        copy_chunks(tmp, num->chunk, nr_chunks);

        /* retrive divisor information */
        get_div_base(&info, base);

        /* 
         * same as naive division, 
         * but without storing the remainders 
         */
        pp = nr_chunks;
        while (pp > 0) {
                size_t done = 0;

                r = chunk_div_naive(tmp, &pp, &info);
                done = div_naive(r, base);

                digits += done;

                if (pp >= 1) {
                        /* test to see the number of divisions */
                        for (; done < info.exp; done++) {
                                digits++;
                        }
                }
                if (pp % 1024 == 0) {
                        eprintf("Done %lu divisions\n", digits);
                }
        }

        xfree(tmp);

        dprintf("size of output will be %lu bytes\n", digits);
        return digits;
}

size_t
sz_approx(const num_t *num, int base)
{
        long int nr;

        size_t digits = 0;
        unsigned long bits;

        nr = ABS(num->size);
        if (nr == 0) {
                return 1;
        }

        bits = get_chunk_bits(num);
        dprintf("Bits %lu (0x%lx)\n", bits, bits);
        dprintf("Base %d given to calculate size\n", base);

        switch (base) {
        case 2:         
                digits = bits;
                break;
        case 4:         
                digits = (bits + 1) / 2;
                break;
        case 8:         
                digits = (bits + 2) / 3;
                break;
        case 16:
                digits = (bits + 3) / 4;
                break;
        case 32:        
                digits = (bits + 4) / 5;
                break;
        default: {
                        /* 
                         * We make a statistical decision:
                         *
                         * If the base_to is smaller that base_from
                         * we return the biggest value, which happens
                         * when converting to base 2 => would result in 
                         * the biggest output string (and roughly ~200% 
                         * bigger than sz() or sz_inv() would return)
                         *
                         * Otherwise use the size of the input string.
                         *
                         * obviously we trade memory for speed
                         */
                        if (base < num->base_from) {
                                /* 
                                 * converting to base 3 would yield a ~50%
                                 * reduction in size 
                                 */
                                if (base > 3) 
                                        digits = (bits + 1) / 2;
                                else
                                        digits = bits;
                        } else {
                                /* maximum we use the same size as the input */
                                digits = num->in_len;
                        }
                }
        }

        dprintf("size of output will be %lu bytes\n", digits);
        return digits;
}

size_t
__convert_str(unsigned char *str, int base,
                unsigned long *chunks, long int nr)
{
        long int cnt = 0;
        struct div_base info;

        unsigned long int r;

        /* retrive div information */
        get_div_base(&info, base);

        dprintf("base=0x%lx, exp=0x%x, shift=0x%x\n",
                        info.base, info.exp, info.shift);

        if (info.shift) {
                dprintf("Got shift=%d (0x%x)\n", 
                                info.shift, info.shift);
        }

        eprintf("%ld chunks to process\n", nr);
        while (nr > 0) {
                size_t done;

                /* 
                 * first retrieve the last remainder as a result
                 * of dividing all chunks with div
                 *
                 * nr will be modifed only when the quotient(s) reaches zero
                 */
                r = chunk_div_naive(chunks, &nr, &info);

                /* and divide this remainder with the base */
                done = __cnv_str(str + cnt, base, r);

                cnt += done;

                /* 
                 * pad with zeros in case we haven't done enough divisions  
                 */
                if (nr >= 1) {
                        for (; done < info.exp; done++) {
                                str[cnt++] = 0; 
                        }
                }
                if (nr % 4096 == 0) {
                        eprintf("\t@ chunk %ld \n", nr);
                }
        }

        /* Reverse order */
        __reverse_order(str, cnt);

        return cnt;
}

char
*convert_str(num_t *num, char *str, int base)
{
        long int nr;
        size_t i, sn;

#if defined APPROX_SIZE
        eprintf("Computing size of the output with approx.\n");
        sn = 1 + sz_approx(num, base);
#else
        eprintf("Computing size of the output with divisions\n");
        sn = 1 + sz(num, base);
#endif

        if (!str) {
                str = xmalloc(1 + sn);
        }

        nr = ABS(num->size);
        dprintf("input number has %ld bytes\n", nr);

        if (nr == 0) {
                str[0] = '0';
                str[1] = '\0';
                return str;
        }

        i = 0;
        if (num->size < 0) {
                str[i++] = '-';
        }

        if (POW_OF_2(base)) {
                unsigned bits;

                dprintf("Base is power of 2\n");

                bits = bits_pow_of_2(base);
                sn = i + __convert_pow_of_2((unsigned char *) str + i, 
                                        bits, num->chunk, nr);
        } else {
                unsigned long *tmp;

                dprintf("Base is not power of 2\n");

                tmp = alloc_chunks(nr);
                copy_chunks(tmp, num->chunk, nr);

                sn = i + __convert_str((unsigned char *) str + i, base, tmp, nr);

                dprintf("converted string has %ld bytes\n", sn);
                xfree(tmp);
        }

        for (; i < sn; i++) {
                str[i] = digits[(unsigned char) str[i]];
        }

        str[sn] = '\0';

        num->out_len = sn;
        return str;
}

char 
*convert(num_t *num, char *str, int base, convert_func conv)
{
        long int nr;
        size_t i, sn;

#if defined APPROX_SIZE
        eprintf("Computing size of the output with approx.\n");
        sn = 1 + sz_approx(num, base);
#else
        eprintf("Computing size of the output with divisions\n");
        sn = 1 + sz(num, base);
#endif

        if (!str) {
                str = xmalloc(1 + sn);
        }

        nr = ABS(num->size);
        dprintf("input number has %ld bytes\n", nr);

        if (nr == 0) {
                str[0] = '0';
                str[1] = '\0';
                return str;
        }

        i = 0;
        if (num->size < 0) {
                str[i++] = '-';
        }

        if (POW_OF_2(base)) {
                unsigned bits;

                dprintf("Base is power of 2\n");

                bits = bits_pow_of_2(base);
                sn = i + __convert_pow_of_2((unsigned char *) str + i, 
                                        bits, num->chunk, nr);
        } else {
                unsigned long *tmp;

                dprintf("Base is not power of 2\n");

                tmp = alloc_chunks(nr);
                copy_chunks(tmp, num->chunk, nr);

                sn = i + conv((unsigned char *) str + i, base, tmp, nr);

                dprintf("converted string has %ld bytes\n", sn);
                xfree(tmp);
        }

        for (; i < sn; i++) {
                str[i] = digits[(unsigned char) str[i]];
        }

        str[sn] = '\0';

        num->out_len = sn;
        return str;
}

/**
 * @}
 */
