#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>
#include <assert.h>

#include "num.h"
#include "log.h"
#include "xutil.h"

static unsigned long
*__realloc_chunks(unsigned long *old, long int size)
{
        assert(size > 0);
        return xrealloc(old, size * sizeof(unsigned long));
}

void
*alloc_chunks(long int size)
{
        return xmalloc(size * sizeof(unsigned long));
}

unsigned long
*num_alloc_chunks(num_t *n, long int size)
{
        n->chunk = alloc_chunks(size);
        n->alloced = size;
        return n->chunk;
}

unsigned long
*num_realloc_chunks(num_t *n, long int size)
{
        size = MAX(size, 1);

        n->chunk = __realloc_chunks(n->chunk, size);
        n->alloced = size;

        if (ABS(n->size) > size) {
                n->size = 0;
        }
        return n->chunk;
}

void 
copy_chunks(unsigned long *d, const unsigned long *s, long int n)
{
        long int i;

        for (i = 0; i < n; i++) {
                d[i] = s[i];
        }
}

void 
num_print_chunks(num_t *num)
{
        int i;

        if (!num->size)
                return;

        eprintf(" [Printing %d chunks]\n", num->size);
        for (i = 0; i < num->size; i++) {
                eprintf("\t@ %d -> chunk=0x%lx\n", i, num->chunk[i]);
        }
}

void 
num_free(num_t *n)
{
        if (n->alloced)
                xfree(n->chunk);
}

void 
num_init(num_t *num)
{
        num->alloced = 0;
        num->chunk = NULL;
        num->size = 0;
}
