V := @

ifeq ($(V),1)
override V = 
endif

ifeq ($(V),0)
override V = @
endif


ARCH := $(shell uname -m)

# init CFLAGS
ifeq ($(ARCH),i686)
$(error "i386 unsupported!")
else
CFLAGS :=
endif

CC = gcc -MD -pipe
OPT_COMP = -O3
DBG = -g

# we use by default sz_approx() to calculate
# the size of the out
OPT_SIZE := -DAPPROX_SIZE

# sanity, everything should be compiled with this by
# default
FORTIFY  := -D_FORTIFY_SOURCE

# POSIX_C_SOURCE >= 1993 to gain access to CLOCK_MONOTONIC
POSIX	 := -D_POSIX_SOURCE -D_POSIX_C_SOURCE=199309L

INCLUDE = -Iinclude/
CFLAGS += -Wall -Wextra -Werror -Wstrict-prototypes -Wmissing-prototypes 
CFLAGS += -ansi $(OPT_COMP) $(OPT_SIZE)

# core files
SRCDIR := src
# applications
APPDIR := bin
# where to dumb the objects
OBJDIR := obj

# debug for gdb or memgrind leaks
ifeq ($(DEBUG), 1)
CFLAGS_DEBUG := $(DBG)
CFLAGS_NEW := $(filter-out $(OPT_COMP),$(CFLAGS))
CFLAGS := $(CFLAGS_NEW) $(CFLAGS_DEBUG)
endif

# + debug messages
ifeq ($(DEBUG), 2)
CFLAGS_DEBUG := $(DBG) -DENABLE_DEBUG
CFLAGS_NEW := $(filter-out $(OPT_COMP),$(CFLAGS))
CFLAGS := $(CFLAGS_NEW) $(CFLAGS_DEBUG)
endif

# sanity with AddressSanitizer
ifeq ($(SANITIZE), 1)
CFLAGS_SANITIZE := $(DBG) -fsanitize=address
CFLAGS_NEW := $(filter-out $(OPT),$(CFLAGS))
CFLAGS := $(CFLAGS_NEW) $(CFLAGS_SANITIZE)
endif

# to remove sz_approx() and use native function
ifeq ($(OPTIMIZE_LEVEL), 0)
CFLAGS := $(filter-out $(OPT_SIZE),$(CFLAGS))
endif

# activate timer for transfer program
# to see how long it took to convert
ifeq ($(TIMER), 1)
CFLAGS += -DENABLE_TIMER $(POSIX)
endif

CORE_SRC := $(SRCDIR)/xutil.c $(SRCDIR)/num.c $(SRCDIR)/ops.c\
	$(SRCDIR)/encode.c $(SRCDIR)/convert.c
CORE_OBJS := $(subst $(SRCDIR),$(OBJDIR),$(CORE_SRC))
CORE_OBJS := $(CORE_OBJS:.c=.o)

CORE_RAND_SRC := $(SRCDIR)/rand.c
CORE_RAND_OBJS := $(OBJDIR)/rand.o

# regression test (run this after screwing with the conversion)
TEST_SRC := $(APPDIR)/test.c
TEST_OBJS := $(subst $(APPDIR),$(OBJDIR),$(TEST_SRC))
TEST_OBJS := $(TEST_OBJS:.c=.o)

# simple conversion from which can be used to convert a number
# supplied on the command line
SCONV_SRC := $(APPDIR)/sconv.c
SCONV_OBJS := $(subst $(APPDIR),$(OBJDIR),$(SCONV_SRC))
SCONV_OBJS := $(SCONV_OBJS:.c=.o)

# converts a randomly generated number from input base to base 2-36 
TIMER_SRC := $(APPDIR)/timer.c
TIMER_OBJS := $(subst $(APPDIR),$(OBJDIR),$(TIMER_SRC))
TIMER_OBJS := $(TIMER_OBJS:.c=.o)

# main program
TRANSFER_SRC := $(APPDIR)/transfer.c
TRANSFER_OBJS := $(subst $(APPDIR),$(OBJDIR),$(TRANSFER_SRC))
TRANSFER_OBJS := $(TRANSFER_OBJS:.c=.o)

# init docs
MARKDOWN := README.md doc/ENCODING.md doc/DIV.md doc/ENC_EXP.md doc/DIV_EXP.md


TARGETS := timer test sconv transfer

all:  $(TARGETS)

timer: $(CORE_OBJS) $(CORE_RAND_OBJS) $(TIMER_OBJS)
	$(V)$(CC) $(CFLAGS) $(POSIX) $(INCLUDE) -o $@ $(TIMER_OBJS) $(CORE_OBJS) $(CORE_RAND_OBJS)
	@echo "CC $@"

sconv: $(CORE_OBJS) $(CORE_RAND_OBJS) $(SCONV_OBJS)
	$(V)$(CC) $(CFLAGS) $(POSIX) $(INCLUDE) -o $@ $(SCONV_OBJS) $(CORE_OBJS) $(CORE_RAND_OBJS)
	@echo "CC $@"

test: $(CORE_OBJS) $(CORE_RAND_OBJS) $(TEST_OBJS)
	$(V)$(CC) $(CFLAGS) $(POSIX) $(INCLUDE) -o $@ $(TEST_OBJS) $(CORE_OBJS) $(CORE_RAND_OBJS)
	@echo "CC $@"

transfer: $(CORE_OBJS) $(TRANSFER_OBJS)
	$(V)$(CC) $(CFLAGS) $(TRANSFER_OBJS) $(CORE_OBJS) -o $@ 
	@echo "CC $@"

run-test: test
	$(shell for b in `seq 5 2 36`; do ./test -f $$b -s 1000 -m naive; done)
	@echo "Test done"

vgrind-test: test
	$(shell valgrind -q --leak-check=full ./test -f 10 -s 10000 -m naive)
	@echo "Valgrind test done"

# default rule for core
$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(shell test ! -d $(OBJDIR) && mkdir $(OBJDIR))
	$(V)$(CC) $(CFLAGS) $(INCLUDE) -c $< -o $@
	@echo "CC [core] $@"

# rand demands POSIX
$(OBJDIR)/rand.o: $(SRCDIR)/rand.c
	$(V)$(CC) $(CFLAGS) $(INCLUDE) $(POSIX) -c $< -o $@
	@echo "CC [core] $@"

# default rule for apps
$(OBJDIR)/%.o: $(APPDIR)/%.c
	$(V)$(CC) $(CFLAGS) $(INCLUDE) -c $< -o $@
	@echo "CC [app] $@"

# timer, test and sconv demands POSIX
$(OBJDIR)/timer.o: $(APPDIR)/timer.c
	$(V)$(CC) $(CFLAGS) $(INCLUDE) $(POSIX) -c $< -o $@
	@echo "CC [app] $@"

$(OBJDIR)/test.o: $(APPDIR)/test.c
	$(V)$(CC) $(CFLAGS) $(INCLUDE) $(POSIX) -c $< -o $@
	@echo "CC [app] $@"

$(OBJDIR)/sconv.o: $(APPDIR)/sconv.c
	$(V)$(CC) $(CFLAGS) $(INCLUDE) $(POSIX) -c $< -o $@
	@echo "CC [app] $@"


cscope.files:
	$(V)rm -rf cscope* TAGS
	$(V)find . -maxdepth 1 -name \*.[chS] > cscope.files

TAGS:	cscope.files
	$(V)ctags -R -L cscope.files

tags:	cscope.files
	$(V)ctags -R -L cscope.files

cscope: cscope.files
	$(V)cscope -b -q -k

pandoc: $(MARKDOWN)
	$(V)pandoc -s --toc -f markdown -t html5 -c pandoc.css README.md -o doc/README.html
	$(shell for j in doc/*.md; do pandoc -s --toc -f markdown -t html5 -c pandoc.css $$j -o $$j.html; done)

doxy: doc/doxygen.conf
	$(V)doxygen doc/doxygen.conf
help:
	@echo "Options:"
	@echo "\tV=1			-- turn on verbose building"
	@echo "\tDEBUG=[12]		-- build with debug symbols and/or debug messages"
	@echo "\tSANITZE=1		-- build with AddressSanitizer"
	@echo "\tTIMER=1			-- enable timing encoding/conversion for main program"
	@echo "\tOPTIMIZE_LEVEL=0	-- disable size approximation"
	@echo "Targets:"
	@echo "\ttimer			-- build only random timer program"
	@echo "\ttest			-- build only random test program"
	@echo "\tsconv			-- build only simple conversion program"
	@echo "\t*transfer*		-- build only main program"
	@echo "\tdoc			-- build doxygen documentation"
	@echo "\tclean			-- clean"
	@echo "Tests:"
	@echo "\trun-test		-- runs test program"
	@echo "\tvgrind-test		-- runs test program under valgrind"

doc: doxy

clean_doc:
	$(V)rm -rf doc/html	

clean: clean_doc
	$(V)rm -rf *.o cscope.* $(TARGETS) $(OBJDIR)/*.o $(OBJDIR)/*.d\
		callgrind* gmon* core.* doc/README.html

