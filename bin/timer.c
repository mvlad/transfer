#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <time.h>

#include "num.h"
#include "log.h"
#include "xutil.h"

#include "encode.h"
#include "convert.h"
#include "timer.h"
#include "rand.h"

unsigned int run_flag = 0;
int base, m_flag;
long int size;
num_t number;

static void 
help(void)
{
        fprintf(stderr, "Usage: ");
        fprintf(stderr, "time [-v] -b base -s size [-m method]\n");
        fprintf(stderr, "\t-v enable verbose\n");
        fprintf(stderr, "\t-h this help message\n");
        fprintf(stderr, "\t-b base to convert   [2-36]\n");
        fprintf(stderr, "\t-s input_size        \n");
        fprintf(stderr, "\t-m method            (default 'naive')\n");
        exit(EXIT_FAILURE);
}

static void 
parse_argv(int argc, char *argv[])
{
        int c;

        if (argc < 3) {
                help();
        }

        while ((c = getopt(argc, argv, "hvb:s:m:")) != -1) {
                switch (c) {
                case 'v':
                        run_flag |= IS_VERB;
                        break;
                case 'h':
                        help();
                        break;
                case 'b':
                        base = atoi(optarg);
                        assert(base > 2 || base < 36);
                        break;
                case 's':
                        size = atol(optarg);   
                        break;
                case 'm': {
                        if (!strncmp(optarg, "naive", 5))
                                m_flag = 1;
                        else
                                help();
                }
                        break;
                default:
                        help();
                        break;
                }
        }
}

int main(int argc, char *argv[])
{
        num_t *a = &number;

        int ret;

        char *s = NULL;
        char *res = NULL;

        char *input = NULL;
        convert_func conv;

        INIT_TIMER(timer1);
        INIT_TIMER(timer2);

        parse_argv(argc, argv);

        switch (m_flag) {
        case 1:
                conv = &__convert_str;
                eprintf(">> converting using naive division\n");
                break;
        default: /* let it slide */
                conv = &__convert_str;
                eprintf("Converting with default naive method\n");
        }

        num_init(a);

        eprintf("+ Generating %ld bytes of random junk\n", size);
        input = gen_digit_str(size);

        eprintf("Encoding... \n");

        START_TIMER(timer1);

        ret = encode_str(a, input, 10);
        if (ret == -1) {
                FATAL("Failed to encode input\n");
        }

        STOP_TIMER(timer1);


        SHOW_WALLCLOCK("Encoding elapsed", timer1);

        eprintf("Converting... \n");

        START_TIMER(timer2);
        s = convert(a, res, base, conv);
        STOP_TIMER(timer2);

        SHOW_WALLCLOCK("Converting elapsed", timer2);

        ADD_TM_TO_TM(timer1, timer2);
        SHOW_WALLCLOCK("Total time", timer1);


        xfree(input);
        xfree(s);
        num_free(a);
        return 0;
}
