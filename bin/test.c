#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "log.h"
#include "num.h"
#include "xutil.h"

#include "timer.h"
#include "test.h"

#include "ops.h"
#include "convert.h"
#include "encode.h"
#include "rand.h"

#define LEN_SIZE        1000

#define BASE_MIN        2
#define BASE_MAX        36


num_t number;

unsigned int run_flag = 0;
int m_flag = 0;
int base_from = 0;
long int size = 0L;

static void 
help(void)
{
        fprintf(stderr, "Usage: ");
        fprintf(stderr, "./test [-v] [-s size] -f base_from [-m method]\n");
        fprintf(stderr, "\t-s size of generated string          \n");
        fprintf(stderr, "\t-f base from which to convert        [2-36]\n");
        fprintf(stderr, "\t-m method                            (default 'naive')\n");
        exit(EXIT_FAILURE);
}

static void 
parse_argv(int argc, char *argv[])
{
        int c;
        if (argc < 3) {
                help();
        }

        while ((c = getopt(argc, argv, "vf:s:m:")) != -1) {
                switch (c) {
                case 'v':
                        run_flag |= IS_VERB;
                        break;
                case 'f':
                        base_from = atoi(optarg);
                        if (base_from < 2 || base_from > 36) {
                                eprintf("Invalid base_from given\n");
                                help();
                        }
                        break;
                case 's':
                        size = atol(optarg);
                        break;
                case 'm': {
                        if (!strncmp(optarg, "naive", 5))
                                m_flag = 1;
                        else
                                help();
                }
                        break;
                default:
                        help();
                        break;
                }
        }
}

int main(int argc, char *argv[])
{
        num_t *a = &number;

        int ret;
        int base;
        char *s1 = NULL;
        char *res1 = NULL;

        char *s2 = NULL;
        char *res2 = NULL;
        convert_func conv;

        parse_argv(argc, argv);
        switch (m_flag) {
        case 1:
                conv = &__convert_str;
                eprintf("Converting using naive method\n");
                break;
        default: /* let it slide */
                conv = &__convert_str;
                eprintf("Converting with default naive method\n");
        }


        char *input = NULL;

        for (base = BASE_MIN; base <= BASE_MAX; base++) {

                if (size) 
                        input = gen_alphanum_str(size, base_from);
                else
                        input = gen_alphanum_str(LEN_SIZE, base_from);

                dprintf("Input: '%s'\n", input);

                num_init(a);

                eprintf("Converting to from base %d to base %d... ", 
                                base_from, base);

                ret = encode_str(a, input, base_from);
                if (ret == -1) {
                        FATAL("Failed to encode input\n");
                }
                s1 = convert(a, res1, base, conv);

                /* no re-alloc, have to free and re-initalize */
                num_free(a);

                /* now convert it back */
                num_init(a);
                ret = encode_str(a, s1, base);
                if (ret == -1) {
                        FATAL("Failed to encode input\n");
                }
                s2 = convert(a, res2, base_from, conv);

                ok1(s2, input);

                xfree(s1);
                xfree(s2);

                num_free(a);
                res1 = res2 = NULL;

                xfree(input);
        }

        return 0;
}

