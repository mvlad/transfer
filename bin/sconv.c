#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>

#include "log.h"
#include "num.h"
#include "xutil.h"

#include "test.h"

#include "ops.h"
#include "convert.h"
#include "encode.h"

#define LEN_SIZE        1000

#define BASE_MIN        2
#define BASE_MAX        36

static const char alphanum[] = "0123456789abcdefghijklmnopqrstuvwxyz";
#define SIZE_ALPHA (sizeof(alphanum)/sizeof(alphanum[0]))

num_t number;
unsigned int run_flag = 0;
int m_flag = 0;

int base_from = 0;
int base_to = 0;

char *def_input = "14867163467071694589724647718821644187074559";
char *input = NULL;
int input_set = 0;

static void help(void)
{
        fprintf(stderr, "Usage: ");
        fprintf(stderr, "./sconv [-v] [-i input] -f base_from -t base_to [-m method]\n");
        fprintf(stderr, "\t-i input string              (default '%s')\n", def_input);
        fprintf(stderr, "\t-f base from to convert      [2-36]\n");
        fprintf(stderr, "\t-t base to convert           [2-36]\n");
        fprintf(stderr, "\t-m method                    (default 'naive')\n");
        exit(EXIT_FAILURE);
}

static void parse_argv(int argc, char *argv[])
{
        int c;
        if (argc < 3) {
                help();
        }

        while ((c = getopt(argc, argv, "vf:t:i:m:")) != -1) {
                switch (c) {
                case 'v':
                        run_flag |= IS_VERB;
                        break;
                case 'f':
                        base_from = atoi(optarg);
                        if (base_from < 2 || base_from > 36) {
                                eprintf("Invalid 'base_from' given\n");
                                help();
                        }
                        break;
                case 't':
                        base_to = atoi(optarg);
                        if (base_to < 2 || base_to > 36) {
                                eprintf("Invalid 'base_to' given\n");
                                help();
                        }
                        break;
                case 'i':
                        input = calloc(strlen(optarg), sizeof(char));
                        xstrcpy(input, optarg);
                        input_set ^= 1;
                        break;
                case 'm': {
                        if (!strncmp(optarg, "naive", 5))
                                m_flag = 1;
                        else
                                help();
                }
                        break;
                default:
                        help();
                        break;
                }
        }
}

int main(int argc, char *argv[])
{
        num_t *a = &number;

        int ret;
        char *s1 = NULL;
        char *res1 = NULL;

        char *s2 = NULL;
        char *res2 = NULL;
        convert_func conv;

        parse_argv(argc, argv);
        switch (m_flag) {
        case 1:
                conv = &__convert_str;
                eprintf("Converting using naive method\n");
                break;
        default: /* let it slide */
                conv = &__convert_str;
                eprintf("Converting with default naive method\n");
        }

        if (!input)
                input = def_input;

        dprintf("Input: '%s'\n", input);

        num_init(a);

        eprintf("Converting to from base %d to base %d... \n", 
                        base_from, base_to);

        ret = encode_str(a, input, base_from);

        eprintf("Encoded chunks\n");
        num_print_chunks(a);
        if (ret == -1) {
                FATAL("Failed to encode input\n");
        }
        s1 = convert(a, res1, base_to, conv);

        eprintf("Result '%s'\n", s1);

        /* no re-alloc, have to free and re-initalize */
        num_free(a);

        /* now convert it back */
        num_init(a);
        ret = encode_str(a, s1, base_to);
        if (ret == -1) {
                FATAL("Failed to encode input\n");
        }
        s2 = convert(a, res2, base_from, conv);

        ok1(s2, input);

        xfree(s1);
        xfree(s2);

        num_free(a);
        res1 = res2 = NULL;

        if (input_set)
                xfree(input);

        return 0;
}
