#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "num.h"
#include "log.h"
#include "xutil.h"
#include "encode.h"
#include "convert.h"

#ifdef ENABLE_TIMER
#include "timer.h"
#endif

unsigned int run_flag = 0;

/** user supplied file */
const char *fin         = NULL;
/** user supplied output file */
const char *fout        = NULL;

/** how many bytes should we read at once */
#define BUFSZ (128 * 1024)

/** start of ouput file */
#define S_STR   "["
/** end of output file */
#define E_STR   "]"


/**
 * @brief store information about the number from the input file
 */
struct line {
        int base_from;
        int base_to;

        /**< length of the base to char array */
        unsigned int base_to_sn;
        /**< base to in a char array */
        char *base_to_str;

         /**< length of number */
        unsigned long int s_num;

        /**< tmp storage for input number */
        char *num;
};

struct line input;
num_t number; 

static void 
line_init(struct line *input)
{
        input->base_from         = 0;
        input->base_to           = 0;

        input->s_num             = 0;
        input->num               = NULL;

        input->base_to_str       = NULL;
        input->base_to_sn        = 0;
}

static void 
line_free(struct line *input)
{
        xfree(input->num);
        xfree(input->base_to_str);
}

static int 
meth_set(void)
{
        if (FLAG_SET(METH_NAIVE)) {
                return 1;
        }
        return 0;
}

static int 
div_meth_present(char *str, int *method)
{
        int i, len = strlen(str);
        struct {
                char *name; 
                int method;
        } div_meths[] = {
                { "naive",      IS_METH_NAIVE   },
                { NULL,         0               }
        };

        for (i = 0; i < 1; i++) {
                if (!strncmp(str, div_meths[i].name, len)) {
                        *method = div_meths[i].method;
                        return 1;
                }
        }
        return 0;
}

static void help(void)
{
        fprintf(stderr, "Usage: transfer [options] input_file output_file\n");
        fprintf(stderr, "Options:\n");
        fprintf(stderr, "\t-h\tthis help message\n");
        fprintf(stderr, "\t-v\tenable verbose messages\n");
#ifdef ENABLE_DEBUG
        fprintf(stderr, "\t-d\tenable debug messages\n");
#endif
#ifdef ENABLE_TIMER
        fprintf(stderr, "\t-t\ttime encoding/division actions\n");
#endif
        fprintf(stderr, "\t-m\tspecify which method for division 'naive' (default)\n");
        exit(EXIT_FAILURE);
}

static void 
parse_argv(int argc, char *argv[])
{
        int p;

        if (argc < 3) {
                goto err;
        }

        for (p = 1; p < argc; p++) {

                if (argv[p] == NULL) {
                        goto err;
                }

                char *str = argv[p];
                if (*str == '-') {

                        str++;

                        char opt = *str++;

                        switch (opt) {
                        case 'h':
                                goto err;
                                break;
#ifdef ENABLE_TIMER
                        case 't':
                                run_flag |= IS_TIME;
                                eprintf("time enabled\n");
                                break;
#endif
                        case 'v':
                                run_flag |= IS_VERB;
                                eprintf("verbose enabled\n");
                                break;
#ifdef ENABLE_DEBUG
                        case 'd':
                                run_flag |= IS_DEBUG;
                                eprintf("debug enabled\n");
                                break;
#endif
                        case 'm': {
                                int found = 0;

                                if (argv[p + 1] == NULL) {
                                        goto err;
                                }

                                char *meth = argv[p + 1];
                                if (!div_meth_present(meth, &found)) {
                                        eprintf("Invalid option '%s'\n", meth);
                                        goto err;
                                }

                                run_flag |= found;
                                if (FLAG_SET(METH_NAIVE)) {
                                        eprintf("doing naive division\n");
                                } else {
                                        eprintf("Invalid flag given!\n");
                                        goto err;
                                }
                                        
                                }
                                continue;
                        default:
                                eprintf("Invalid option '%c'\n", opt);
                                goto err;
                                break;
                        }
                } else {

                        if (!fin) {
                                if (meth_set())
                                        fin = argv[p + 1];
                                else
                                        fin = str;
                                /*
                                 * we continue as not to mess with fout
                                 */
                                continue;
                        }
                        if (!fout) {
                                if (meth_set())
                                        fout = argv[p + 1];
                                else
                                        fout = str;
                                /* 
                                 * we continue as the optarg for -m might be at the end
                                 */
                                continue;
                        }
                }
        }

        if (!fin || !fout) {
                eprintf("Invalid option, you haven't supplied "
                        "an output file!\n");
                goto err;
        }
        
        return;
err:
        help();
}

/**
 * @brief determine the size of the input and allocate storage for it
 * @param fn the input filename
 * @param linebuf storage for the line
 * @retval how many bytes have been alloced into linebuf
 * @retval \c -1 if failure
 */
static unsigned int 
alloc_line_info(const char *fn, char **linebuf)
{
        int fd; size_t nread;
        unsigned int alloc = 0;

        if ((fd = open(fn, O_RDONLY)) == -1) {
                eprintf("# failed to open file: '%s'\n", fn);
                goto err;
        }

        char tbuf[BUFSZ];
        memset(tbuf, 0, BUFSZ);

        while ((nread = xread(fd, tbuf, BUFSZ)) > 0) {
                alloc += nread;
        }

        if (!alloc) {
                eprintf("# file is empty!\n");
                goto err;
        }

        if (close(fd) == -1) {
                goto err;
        }

        *linebuf = xmalloc(alloc + 1);
        memset(*linebuf, 0, alloc + 1);

        return alloc;
err:
        return -1;
}

/**
 * @brief read the contents of fn into string
 * @param fn filename
 * @param linebuf a buffer storing the line, caller
 * is responsible for free'ign up after usage
 * @return the filesize == how much we've allocated
 *
 * @note we open the file twice, to adjust the offset
 */
static int
read_file(const char *fn, char **linebuf)
{
        int fd, alloc = 0;
        ssize_t filesz = 0;

        if ((alloc = alloc_line_info(fn, linebuf)) == -1) {
                goto err;
        }

        if ((fd = open(fn, O_RDONLY)) == -1) {
                eprintf("# failed to open file: '%s'\n", fn);
                goto err;
        }

        /* now that we know how big it is, slurp it */
        filesz = xread(fd, *linebuf, alloc);

        if (close(fd) == -1) {
                goto err;
        }

        if (filesz != alloc) {
                eprintf("# failed to read() "
                        "filesz=%ld, alloc=%d\n", filesz, alloc);
                goto err;
        }

        return filesz;
err:
        return -1;
}

/**
 * @brief extract base_to, base_from and the number
 * @param data where to store base_from, base_to and the number
 * @param fn the input filename
 * @retval 0 on success
 * @retval -1 on failure
 *
 * @note we assume that both the input base and output base
 * is in decimal!
 */
static int
get_num(struct line *data, const char *fn)
{
        char *linebuf;
        int base_from, base_to;

        /* get base_to directly into a char array */
        char base_to_str[3];
        size_t base_to_sn = 0;

        ssize_t filesz = 0;
        size_t numsz = 0;

        linebuf = NULL;
        base_from = base_to = 0;

        filesz = read_file(fn, &linebuf);
        if (filesz == -1) {
                goto err;
        }

        char *buf, *n_start, *n_end;
        buf = n_start = n_end = NULL;
        /* 
         * extract from the input ([NUMBER]BASE_FROM=BASE_TO) 
         * NUMBER, BASE_FROM and BASE_TO
         */
        for (buf = linebuf; buf && *buf && filesz >= 0; buf++, filesz--) {

                /* accept also files which end with newline */
                if (*buf == '\n')
                        break;

                /* start getting the number */
                if (*buf == '[') {
                        n_start = buf;
                        /* eat '[' */
                        n_start++;
                }

                /* the number has endeed */
                if (*buf == ']') {

                        n_end = buf; 
                        numsz = n_end - n_start;

                        /* eat ']' */
                        n_end++;

                        /* retrive base from */
                        while (n_end && *n_end != '=') {
                                base_from = (base_from * 10) + *n_end - '0';
                                n_end++;
                        }
                }

                /* determine in which base needs to be converted */
                if (*buf == '=') {
                        int i = 0;

                        char *end = buf; 
                        end++; 

                        memset(base_to_str, 0, 3);
                        while (end && *end) {
                                if (i > 3) {
                                        eprintf("# invalid base_to\n");
                                        goto err;
                                }

                                if (*end == '\n') {
                                        break;
                                }

                                /* retrive base to */
                                base_to = (base_to * 10) + *end - '0';

                                /* and keep it as char array */
                                base_to_str[i++] = *end;
                                end++;
                        }
                        base_to_sn = i;
                }
        }

        if (!numsz) {
                /* failed to get size no */
                eprintf("# invalid size for the number\n");
                goto err;
        }

        if ((base_from < 2 || base_from > 35) ||
            (base_to < 2 || base_to > 35)) {
                eprintf("Invalid base\n");
                goto err;
        }

        data->s_num = numsz;
        data->base_from = base_from;
        data->base_to = base_to;

        buf = linebuf; buf++;

        data->num = xmalloc(numsz + 1);
        memcpy(data->num, buf, numsz);

        data->num[numsz] = '\0';

        data->base_to_str = xmalloc(base_to_sn + 1);
        memcpy(data->base_to_str, base_to_str, base_to_sn);
        
        data->base_to_str[base_to_sn] = '\0';
        data->base_to_sn = base_to_sn;

        xfree(linebuf);
        return 0;

err:
        if (linebuf) {
                xfree(linebuf);
        }
        eprintf("Failed to extract number...\n");
        return -1;
}


static int
write_out(const char *fout, struct line *data, char *res, size_t len)
{
        int fdout; size_t nwrite;

        int wflags = S_IWUSR | S_IRUSR;
        int wmode = O_CREAT | O_WRONLY;

        if ((fdout = open(fout, wmode, wflags)) == -1) {
                goto err_exit;
        }

        /* prologue */
        nwrite = xwrite(fdout, S_STR, 1);
        if (nwrite != 1) {
                goto err_exit;
        }

        /* write the main contents */
        if (xwrite(fdout, res, len) != len) {
                goto err_exit;
        }

        /* epilogue */
        nwrite = xwrite(fdout, E_STR, 1);
        if (nwrite != 1) {
                return -1;
        }

        /* base to */
        nwrite = xwrite(fdout, data->base_to_str, data->base_to_sn);
        if (nwrite != data->base_to_sn) {
                return -1;
        }

        if (close(fdout) == -1) {
                return -1;
        }

        return 0;
err_exit:
        eprintf("# failed to write to %s\n", fout);
        return -1;
}




/**
 * @brief process input file, transfer, and write to output file
 *
 * @note
 * - we can't rely on a \c stat(2) to retrive the filesz
 * - we can't rely on \c lseek64(2) to manipulate the offset
 *   => thus we open/read the file twice, once to determine
 *   the filesz and alloc tmp storage used the second
 *   time, for extracting data.
 * We can probably improve the situation by determining the
 * input string from the first try.
 */

int main(int argc, char *argv[])
{
        char *__res, *res;

        int ret; 
        struct line *data = &input;
        num_t *a = &number;

        convert_func conv;
        res = __res = NULL;

#if defined ENABLE_TIMER
        INIT_TIMER(timer_encode);
        INIT_TIMER(timer_convert);
#endif

        parse_argv(argc, argv);

        line_init(data);

        ret = get_num(data, fin);
        if (ret == -1) {
                FATAL("Failed to extract number!\n");
        }

        if (!meth_set()) {
                conv = __convert_str;
        }

        num_init(a);

#if defined ENABLE_TIMER
        START_TIMER(timer_encode);
#endif
        eprintf("encoding string from base %d\n", data->base_from);
        ret = encode_str(a, data->num, data->base_from);
        if (ret == -1) {
                eprintf("Failed to encode number!\n");
                goto err_exit;
        }

#if defined ENABLE_TIMER
        STOP_TIMER(timer_encode);
#endif

#if defined ENABLE_TIMER
        START_TIMER(timer_convert);
#endif
        eprintf("converting to base %d\n", data->base_to);
        res = convert(a, __res, data->base_to, conv);
        if (!res) {
                eprintf("Failed to convert number!\n");
                goto err_exit;
        }
#if defined ENABLE_TIMER
        STOP_TIMER(timer_convert);
#endif

        ret = write_out(fout, data, res, a->out_len);
        if (ret == -1) {
                eprintf("Failed to write converted number!\n");
                goto err_exit;
        }

        xfree(res);
        num_free(a);
        line_free(data);

        eprintf("Number converted!\n");

#if defined ENABLE_TIMER
        SHOW_WALLCLOCK("Encoding took", timer_encode);
        SHOW_WALLCLOCK("Conversion took", timer_convert);

        ADD_TM_TO_TM(timer_encode, timer_convert);
        SHOW_WALLCLOCK("Total time ", timer_encode);
#endif

        return 0;

err_exit:
        dprintf("Exiting abruptly...\n");
        if (res) {
                xfree(res);
        }

        num_free(a);
        line_free(data);

        exit(EXIT_FAILURE);
}
