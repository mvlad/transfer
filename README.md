Transfer Number
===============

Simple program to convert a number from a given base to another, using as
a input, a file which contains: `[NUMBER]BASE_FROM=BASE_TO`.
The result of the transformation to the new base will be stored in a user
supplied file given at run-time, under the form `[CONVERTED_NUMBER]BASE_TO`.
Both `BASE_TO` and `BASE_FROM` are presumed to be between 2 and 35 (in decimal),
inclusive.

Example:

| Input                           | Output            |
|---------------------------------|-------------------|
| `[11010101010101000011]2=10`    | `[873795]10`      |
| `[873795]10=16`                 | `[d5543]16`       |
| `[d5543]16=8`                   | `[3252503]8`      |


Requirements
------------

- for I/O  `write(2)` and `read(2)`
- for dynamic allocation use `malloc(3)/free(3)`
- should be able to compute very long inputs with very long digits 

Building & Running
------------------

### Depends ###

* gmake
* some programs require `getopt(3)` with `_POSIX_C_SOURCE=2`
* `CLOCK_MONOTONIC` requires `_POSIX_C_SOURCE` with at least `199309L` (assumes
   `_POSIX_C_SOURCE=2`)
* `x86_64` -- currently no IA-32 support

### Building ###

* To build all programs with default options, just invoke `make`
* To see the list of available targets and options `make help`

#### Options ####

* Show verbose build with `make V=1`
* Default `CFLAGS` and optimizations
    - default we build with `FORTIFY_SOURCE=2`, `-O3` and `OPTIMIZE_LEVEL=1`
* Debug builds
    - build with debugging symbols use `DEBUG=1`
    - build with debugging symbols and debug messages `DEBUG=2`
* Sanitize
    - you require a gcc 4.8 version to order to use
    [Address Sanitizer](https://code.google.com/p/address-sanitizer/)
    - build with `make SANITIZE=1`
* Optimizations enabled -- disable with `OPTIMIZE_LEVEL=0`
    - by default we cheat and calculate the size of the converted
number by making a few assumptions, instead of performing division
operations (see [division](doc/DIV.md))
* Timer -- to see how long it took to convert an number.
For the *main* program build with `TIMER=1`.

#### Targets ####

* test -- regression type of test with randomly generated data, 
useful in case we screw with the conversion functions. Performs
back-and-forth conversion to test the conversions.
* time -- speed test, randomly generate input on user-supplied
size and display the time it took for encoding and conversion.
* *sconv* -- simple conversion tool that takes the number as a
parameter
* **transfer** -- **main** program which takes an input file and an
output file.
* all -- all of the above programs

### Running ###

*Transfer*

    $ ./transfer /path/to/input /path/to/output

*Convert a number from base 10 to base 33 with verbose output*

    $ ./sconv -v -f 10 -t 33 -m naive -i "14867163467071694589724647718821644187074559"
    (>) Converting using naive division
    (>) Converting to from base 10 to base 33... 
    (>) Encoded chunks
    (>)  [Printing 3 chunks]
    (>)     @ 0 -> chunk=0xbbbbddddddddffff
    (>)     @ 1 -> chunk=0xaaaaeeeeeeeebbbb
    (>)     @ 2 -> chunk=0xaaaa
    (>) Result '4gnigjqhbjs4i9lee43demvue588i'
    (>) OK!

*Convert a number from base 33 to base 16*

    ./sconv -v -f 33 -t 16 -m naive -i "4gnigjqhbjs4i9lee43demvue588i"
    (>) Converting using naive division
    (>) Converting to from base 33 to base 16... 
    (>) Encoded chunks
    (>)  [Printing 3 chunks]
    (>)     @ 0 -> chunk=0xbbbbddddddddffff
    (>)     @ 1 -> chunk=0xaaaaeeeeeeeebbbb
    (>)     @ 2 -> chunk=0xaaaa
    (>) Result 'aaaaaaaaeeeeeeeebbbbbbbbddddddddffff'
    (>) OK!

Benchmarking tool!
------------------

Arch: i7-3720QM CPU @ 2.60GHz, randomly generated number in base 10. Conversion
to base 23.

| Size (digits) | # Chunks  | Mem Usage     |   Running Time                            |
|---------------|-----------|---------------|-------------------------------------------|
| 10k           | 520       | -             |   Total: 4ms      (0.004000s, 0.000067m)  |
| 100k          | 5191      | 604 kB        |   Total: 481ms    (0.481000s, 0.008017m)  |
| 1M            | 51906     | 3.2M          |   Total: 47707ms  (47.707000s, 0.795117m) |
| 10M           | 519052    | 27.5M         |         Too FLong                         |

Arch: i7-2600 CPU @ 3.40GHz, randomly generated number in base 10. Conversion 
to base 23.


| Size (digits) | # Chunks  | Mem Usage     |   Running Time                                |
|---------------|-----------|---------------|-----------------------------------------------|
| 10k           | 520       | -             |   Total: 3ms (0.003000s, 0.000050m)           |
| 100k          | 5191      | 604 kB        |   Total: 333ms (0.333000s, 0.005550m)         |
| 1M            | 51906     | 3.2M          |   Total: 33498ms (33.498000s, 0.558300m)      |
| 10M           | 519052    | 27.5M         |   Total: 3446616ms (3446.616000s, 57.443600m) |

Internals
---------

* [Internal number representation](doc/ENCODING.md)
* [Internal division operations](doc/DIV.md)
